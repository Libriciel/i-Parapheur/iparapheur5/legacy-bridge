/*
 * Legacy bridge
 * Copyright (C) 2020-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.legacybridge.endpoint;


import org.springframework.stereotype.Controller;

import javax.jws.HandlerChain;
import javax.jws.WebService;
import javax.xml.ws.soap.MTOM;

import static coop.libriciel.legacybridge.utils.StringUtils.TARGET_NAMESPACE;

@MTOM
@HandlerChain(file = "handlers.xml")
@WebService(
        endpointInterface = "coop.libriciel.legacybridge.generated.InterfaceParapheur",
        serviceName = "InterfaceParapheurService",
        portName = "InterfaceParapheurSoap11",
        targetNamespace = TARGET_NAMESPACE
)
@Controller
public class InterfaceParapheurEndPointMtom extends InterfaceParapheurImpl {

}
