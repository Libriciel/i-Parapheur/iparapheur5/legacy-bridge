/*
 * Legacy bridge
 * Copyright (C) 2020-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.legacybridge.endpoint;

import coop.libriciel.legacybridge.configuration.WebApplicationContextLocator;
import coop.libriciel.legacybridge.generated.*;
import coop.libriciel.legacybridge.models.exceptions.FolderRetrievalException;
import coop.libriciel.legacybridge.services.IparapheurServiceInterface;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.stereotype.Controller;
import org.springframework.web.context.WebApplicationContext;

@Controller
public class InterfaceParapheurImpl implements InterfaceParapheur {

    @Autowired
    private IparapheurServiceInterface iparapheurService;

    public InterfaceParapheurImpl() {
        AutowiredAnnotationBeanPostProcessor bpp = new AutowiredAnnotationBeanPostProcessor();
        WebApplicationContext currentContext = WebApplicationContextLocator.getCurrentWebApplicationContext();
        bpp.setBeanFactory(currentContext.getAutowireCapableBeanFactory());
        bpp.processInjection(this);
    }

    @Override
    public EffacerDossierRejeteResponse effacerDossierRejete(String effacerDossierRejeteRequest) {
        return this.iparapheurService.deleteRejectedFolder(effacerDossierRejeteRequest);
    }

    @Override
    public GetListeTypesResponse getListeTypes(Object getListeTypesRequest) {
        return this.iparapheurService.getTypeList();
    }

    @Override
    public GetListeMetaDonneesResponse getListeMetaDonnees(Object getListeMetaDonneesRequest) {
        return this.iparapheurService.getMetadataList();
    }

    @Override
    public CreerDossierResponse creerDossier(CreerDossierRequest creerDossierRequest) {
        return this.iparapheurService.createDraft(creerDossierRequest);
    }

    @Override
    public CreerDossierPESResponse creerDossierPES(CreerDossierPESRequest creerDossierPESRequest) {
        return this.iparapheurService.createPESFolder(creerDossierPESRequest);
    }

    @Override
    public RechercherDossiersResponse rechercherDossiers(RechercherDossiersRequest rechercherDossiersRequest) {
        return this.iparapheurService.searchFolders(rechercherDossiersRequest);
    }

    @Override
    public GetDossierResponse getDossier(String getDossierRequest) {
        return this.iparapheurService.getFolder(getDossierRequest);
    }

    @Override
    public EnvoyerDossierTdTResponse envoyerDossierTdT(String envoyerDossierTdTRequest) {
        return this.iparapheurService.sendTdTFolder(envoyerDossierTdTRequest);
    }

    @Override
    public ForcerEtapeResponse forcerEtape(ForcerEtapeRequest forcerEtapeRequest) {
        return this.iparapheurService.forceStep(forcerEtapeRequest);
    }

    @Override
    public GetClassificationActesTdtResponse getClassificationActesTdt(GetClassificationActesTdtRequest getClassificationActesTdtRequest) {
        return this.iparapheurService.getActesClassification(getClassificationActesTdtRequest);
    }

    @Override
    public EnvoyerDossierPESResponse envoyerDossierPES(String envoyerDossierPESRequest) {
        return this.iparapheurService.sendPESFolder(envoyerDossierPESRequest);
    }

    @Override
    public ArchiverDossierResponse archiverDossier(ArchiverDossierRequest archiverDossierRequest) {
        return this.iparapheurService.archiveFolder(archiverDossierRequest);
    }

    @Override
    public GetCircuitResponse getCircuit(GetCircuitRequest getCircuitRequest) {
        return this.iparapheurService.getWorkflow(getCircuitRequest);
    }

    @Override
    public String echo(String echoRequest) {
        return this.iparapheurService.echoRequest(echoRequest);
    }

    @Override
    public GetListeSousTypesResponse getListeSousTypes(String getListeSousTypesRequest) {
        return this.iparapheurService.getSubtypeList(getListeSousTypesRequest);
    }

    @Override
    public GetHistoDossierResponse getHistoDossier(String getHistoDossierRequest) {
        return this.iparapheurService.getFolderHistory(getHistoDossierRequest);
    }

    @Override
    public ExercerDroitRemordDossierResponse exercerDroitRemordDossier(String exercerDroitRemordDossierRequest) {
        return this.iparapheurService.exerciseFolderRemorseRight(exercerDroitRemordDossierRequest);
    }

    @Override
    public GetStatutTdTResponse getStatutTdT(String getStatutTdTRequest) {
        return this.iparapheurService.getTdtStatus(getStatutTdTRequest);
    }

    @Override
    public EnvoyerDossierMailSecuriseResponse envoyerDossierMailSecurise(EnvoyerDossierMailSecuriseRequest envoyerDossierMailSecuriseRequest) {
        return this.iparapheurService.sendFolderSecureMail(envoyerDossierMailSecuriseRequest);
    }

    @Override
    public IsUtilisateurExisteResponse isUtilisateurExiste(String isUtilisateurExisteRequest) {
        return this.iparapheurService.doesUserExists(isUtilisateurExisteRequest);
    }

    @Override
    public GetListeTypesPourUtilisateurResponse getListeTypesPourUtilisateur(String getListeTypesPourUtilisateurRequest) {
        return this.iparapheurService.getUserTypeList(getListeTypesPourUtilisateurRequest);
    }

    @Override
    public GetListeSousTypesPourUtilisateurResponse getListeSousTypesPourUtilisateur(GetListeSousTypesPourUtilisateurRequest getListeSousTypesPourUtilisateurRequest) {
        return this.iparapheurService.getUserSubTypeList(getListeSousTypesPourUtilisateurRequest);
    }

    @Override
    public GetCircuitPourUtilisateurResponse getCircuitPourUtilisateur(GetCircuitPourUtilisateurRequest getCircuitPourUtilisateurRequest) {
        return this.iparapheurService.getUserWorkflow(getCircuitPourUtilisateurRequest);
    }

    @Override
    public GetMetaDonneesRequisesPourTypeSoustypeResponse getMetaDonneesRequisesPourTypeSoustype(GetMetaDonneesRequisesPourTypeSoustypeRequest getMetaDonneesRequisesPourTypeSoustypeRequest) {
        return this.iparapheurService.getRequiredMetadataForTypeAndSubtype(getMetaDonneesRequisesPourTypeSoustypeRequest);
    }
}
