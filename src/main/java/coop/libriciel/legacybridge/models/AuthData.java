/*
 * Legacy bridge
 * Copyright (C) 2020-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.legacybridge.models;

import coop.libriciel.iparapheur.legacy.model.DeskRepresentation;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class AuthData {
    String authToken;
    String adminAuthToken;
    boolean isAuthenticated;
    String tenantId;
    DeskRepresentation desk;
    coop.libriciel.iparapheur.internal.ApiClient internalDefaultClient;
    coop.libriciel.iparapheur.legacy.ApiClient legacyDefaultClient;
    coop.libriciel.iparapheur.standard.ApiClient standardDefaultClient;
    coop.libriciel.iparapheur.provisioning.ApiClient provisioningDefaultClient;

    public boolean isAuthenticatedWithOneDesk() {
        return isAuthenticated && desk != null;
    }
}
