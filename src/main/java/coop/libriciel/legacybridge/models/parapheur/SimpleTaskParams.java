/*
 * Legacy bridge
 * Copyright (C) 2020-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.legacybridge.models.parapheur;

import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class SimpleTaskParams {
    public static final String API_NAME = "body";
    public static final String API_VALUE =
            """
            The public annotation is visible to any users, and will be stored in task history.
            It is mandatory for any reject, and should be longer than 3 characters.

            The private annotation will only be sent to the workflow's next user.
            It won't be archived anywhere.
            """;

    protected String publicAnnotation;
    protected String privateAnnotation;
}
