/*
 * Legacy bridge
 * Copyright (C) 2020-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.legacybridge.models.parapheur;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import coop.libriciel.iparapheur.legacy.model.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.http.MediaType;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
@JsonIgnoreProperties("mainDocument")
public class Document {

    private String id;
    private String name;
    private Integer index;
    private int pageCount;
    private Long contentLength;
    private Object contentFlux;
    private MediaType mediaType;
    private String pdfVisualId;
    @Default
    private List<SignaturePlacement> signaturePlacementAnnotations = new ArrayList<>();
    @Default
    private Map<String, PdfSignaturePosition> signatureTags = new HashMap<>();
    @Default
    private Map<String, PdfSignaturePosition> sealTags = new HashMap<>();
    @Default
    private List<DetachedSignature> detachedSignatures = new ArrayList<>();
    @Default
    private List<ValidatedSignatureInformation> embeddedSignatureInfos = new ArrayList<>();
    @Default
    private Map<String, PageInfo> pagesProperties = new HashMap<>();
    private Boolean deletable;
    private Boolean isMainDocument;
    private boolean isSignatureProof = false;
    // Temp fix to be sure to avoid any deserialization problem
    private boolean isInternal = false;
}

