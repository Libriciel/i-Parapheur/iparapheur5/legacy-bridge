/*
 * Legacy bridge
 * Copyright (C) 2020-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.legacybridge.models.parapheur;

import coop.libriciel.iparapheur.legacy.model.*;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Builder.Default;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.*;


@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Folder {
    protected String id;

    @Default
    protected String legacyId = null;

    protected String name;

    protected Date dueDate;

    @Default
    protected Map<String, String> metadata = new HashMap<>();

    protected Date draftCreationDate;

    protected TypeDto type;

    protected SubtypeDto subtype;

    protected UserRepresentation originUser;

    protected DeskRepresentation originDesk;

    protected DeskRepresentation finalDesk;

    private Boolean isReadByCurrentUser;
    private Boolean readByCurrentUser;

    protected String typeId;

    protected String subtypeId;

    @Default
    protected List<Task> stepList = new ArrayList<>();
    @Default
    protected List<Document> documentList = new ArrayList<>();
    @Default
    protected List<SignatureProof> signatureProofList = new ArrayList<>();
    @Default
    protected Set<String> readByUserIds = new HashSet<>();

    protected FolderVisibility visibility;
}
