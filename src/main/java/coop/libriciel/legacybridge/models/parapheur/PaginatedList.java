/*
 * Legacy bridge
 * Copyright (C) 2020-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.legacybridge.models.parapheur;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;

import java.util.List;
import java.util.function.LongSupplier;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class PaginatedList<T> {

    public static final String API_DOC_PAGE = "Result page, starting at 0";
    public static final String API_DOC_PAGE_DEFAULT = "0";
    public static final String API_DOC_PAGE_SIZE = "Result chunk size\n(Note that every retrieved chunk's data may be roughly 0.5ko)";
    public static final String API_DOC_PAGE_SIZE_DEFAULT = "50";
    public static final String API_DOC_SORT_BY = "Sorting parameter";
    public static final String API_DOC_ASC = "Results sorted in ascending/descending order";
    public static final String API_DOC_ASC_DEFAULT = "true";


    private List<T> data;
    private long page;
    private long pageSize;
    private long total;



    /**
     * Constructor with a lazy total supplier, that will be called only if necessary.
     *
     * @param data          The final list, should not be modified afterwards
     * @param page          Starting at 0
     * @param pageSize      Page chunk size
     * @param totalSupplier The lazy evaluator
     */
    public PaginatedList(@NotNull List<T> data, long page, long pageSize, @NotNull LongSupplier totalSupplier) {

        this.data = data;
        this.page = page;
        this.pageSize = pageSize;

        // Managing computable cases, avoiding unnecessary evaluations

        if ((data.size() == 0) && (page == 0)) {
            total = 0;
        } else if ((data.size() != 0) && (data.size() != pageSize)) {
            total = page * pageSize + data.size();
        } else {
            total = totalSupplier.getAsLong();
        }
    }

}
