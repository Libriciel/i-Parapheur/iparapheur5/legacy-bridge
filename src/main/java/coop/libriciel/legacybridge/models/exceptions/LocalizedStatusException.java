/*
 * Legacy bridge
 * Copyright (C) 2020-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.legacybridge.models.exceptions;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.annotations.PropertyKey;
import org.springframework.http.HttpStatus;
import org.springframework.web.server.ResponseStatusException;

import java.text.MessageFormat;
import java.util.Locale;
import java.util.ResourceBundle;

public class LocalizedStatusException extends ResponseStatusException {
    public static final String MESSAGE_BUNDLE = "messages";


    public static String getMessageForLocale(@PropertyKey(resourceBundle = MESSAGE_BUNDLE) String messageKey, @NotNull Locale locale, @Nullable Object... stringArgs) {

        String message = ResourceBundle
                .getBundle(MESSAGE_BUNDLE, locale)
                .getString(messageKey);

        return MessageFormat.format(message, stringArgs);
    }


    public LocalizedStatusException(HttpStatus status, @PropertyKey(resourceBundle = MESSAGE_BUNDLE) String messageKey, @Nullable Object... stringArgs) {
        super(status, getMessageForLocale(messageKey, Locale.getDefault(), stringArgs));
    }

}
