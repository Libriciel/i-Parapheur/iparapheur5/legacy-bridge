/*
 * Legacy bridge
 * Copyright (C) 2020-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.legacybridge.models.exceptions;

import java.io.IOException;

/**
 * Exception raised when a coop.libriciel.legacybridge.models.parapheur.Folder (or a part of it like a document)
 * cannot be retrieved from the source parapheur.
 * <p>
 * The message should be suitable to be sent back as response through the legacy soap api
 */
public class FolderRetrievalException extends IOException {

    static final long serialVersionUID = 7818375828146090145L;

    public FolderRetrievalException(String message) {
        super(message);
    }


}
