/*
 * Legacy bridge
 * Copyright (C) 2020-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.legacybridge.configuration;

import com.sun.xml.ws.transport.Headers;
import lombok.extern.log4j.Log4j2;

import javax.servlet.http.HttpServletResponse;
import javax.xml.namespace.QName;
import javax.xml.soap.SOAPException;
import javax.xml.soap.SOAPMessage;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Log4j2
public class SoapMessageInterceptor implements SOAPHandler<SOAPMessageContext> {
    @Override
    public boolean handleMessage(SOAPMessageContext context) {
        log.info("***handleMessage***");
        logSoapMessage(context);
        return true;
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        log.debug("***handleFault***");
        logSoapMessage(context);
        return false;
    }

    @Override
    public void close(MessageContext context) {
        log.debug("_______________close_____________ ");
    }

    @Override
    public Set<QName> getHeaders() {
        return new HashSet<>();
    }

    private void logSoapMessage(SOAPMessageContext context) {
        if (!log.isDebugEnabled()) {
            return;
        }
        Boolean isOutBound = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
        SOAPMessage soapMsg = context.getMessage();
        try {
            if (isOutBound) {
                log.debug("Intercepting outbound message:");
            } else {
                log.debug("Intercepting inbound message:");
                List<String> authHeaders = ((Headers) context.get(MessageContext.HTTP_REQUEST_HEADERS))
                        .get("Authorization");

                if (authHeaders == null) {
                    HttpServletResponse response = (HttpServletResponse) context.get(MessageContext.SERVLET_RESPONSE);
                    response.sendError(401);
                    throw new RuntimeException("Unauthorized");
                }

            }

            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            soapMsg.writeTo(baos);

            if (baos.size() < 8192) {
                log.debug(baos);
            } else {
                log.debug("big payload detected, set the log level to TRACE to display them anyway");
                log.trace(baos);
            }

            log.debug("\n________________________________");

        } catch (SOAPException | IOException e) {
            log.debug(e);
        }
    }

}
