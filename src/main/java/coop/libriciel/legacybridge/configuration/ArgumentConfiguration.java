/*
 * Legacy bridge
 * Copyright (C) 2020-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.legacybridge.configuration;

import coop.libriciel.legacybridge.configuration.properties.ServiceAuthProperties;
import coop.libriciel.legacybridge.configuration.properties.ServiceParapheurProperties;
import coop.libriciel.legacybridge.models.exceptions.BadConfigurationException;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class ArgumentConfiguration {


    private final ServiceParapheurProperties serviceParapheurProperties;
    private final ServiceAuthProperties serviceAuthProperties;


    private @Value("${keycloak.resource}")
    String clientId;

    private @Value("${keycloak.auth-server-url}")
    String authServer;

    private @Value("${keycloak.realm}")
    String realm;


    @Autowired
    public ArgumentConfiguration(ServiceAuthProperties authProperties,
                                 ServiceParapheurProperties parapheurProperties) {
        this.serviceAuthProperties = authProperties;
        this.serviceParapheurProperties = parapheurProperties;
    }


    @Bean
    public void testVariables() throws Exception {

        if (StringUtils.isEmpty(serviceParapheurProperties.getHost())) {
            throw new BadConfigurationException("messages.check_variable_service_parapheur_host");
        }
        if (serviceParapheurProperties.getPort() == null) {
            throw new BadConfigurationException("messages.check_variable_service_parapheur_port");
        }
        if (StringUtils.isEmpty(serviceParapheurProperties.getUrl())) {
            throw new BadConfigurationException("messages.check_variable_service_parapheur_url");
        }
        if (StringUtils.isEmpty(serviceParapheurProperties.getProtocol())) {
            throw new BadConfigurationException("messages.check_variable_service_parapheur_protocol");
        }

        if (StringUtils.isEmpty(serviceAuthProperties.getLogin())) {
            throw new BadConfigurationException("messages.check_variable_service_auth_login");
        }
        if (StringUtils.isEmpty(serviceAuthProperties.getPassword())) {
            throw new BadConfigurationException("messages.check_variable_service_auth_password");
        }

        if (clientId == null || clientId.isEmpty()) {
            throw new BadConfigurationException("messages.check_variable_keycloak_resource");
        }
        if (authServer == null || authServer.isEmpty()) {
            throw new BadConfigurationException("messages.check_variable_keycloak_auth_server_url");
        }
        if (realm == null || realm.isEmpty()) {
            throw new BadConfigurationException("messages.check_variable_keycloak_realm");
        }
    }


}

