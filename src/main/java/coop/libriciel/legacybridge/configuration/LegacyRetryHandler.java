/*
 * Legacy bridge
 * Copyright (C) 2020-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.legacybridge.configuration;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import com.sun.xml.messaging.saaj.util.ByteOutputStream;
import com.sun.xml.ws.transport.Headers;
import coop.libriciel.legacybridge.configuration.properties.ServiceParapheurProperties;
import coop.libriciel.legacybridge.generated.*;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.collections4.CollectionUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.AutowiredAnnotationBeanPostProcessor;
import org.springframework.oxm.jaxb.Jaxb2Marshaller;
import org.springframework.stereotype.Component;
import org.springframework.web.context.WebApplicationContext;
import org.springframework.ws.WebServiceMessage;
import org.springframework.ws.client.core.support.WebServiceGatewaySupport;
import org.springframework.ws.soap.saaj.SaajSoapMessage;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.namespace.QName;
import javax.xml.soap.MimeHeaders;
import javax.xml.soap.SOAPException;
import javax.xml.ws.handler.MessageContext;
import javax.xml.ws.handler.soap.SOAPHandler;
import javax.xml.ws.handler.soap.SOAPMessageContext;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Map;
import java.util.Optional;
import java.util.Set;

import static java.util.Collections.emptyMap;
import static org.apache.commons.collections4.MapUtils.getMap;
import static org.apache.commons.collections4.MapUtils.getString;


@Log4j2
@Component
public class LegacyRetryHandler extends WebServiceGatewaySupport implements SOAPHandler<SOAPMessageContext> {
    private static final String REQUEST_XML = "REQUEST_XML";
    private static final String PACKAGES_TO_SCAN = "coop.libriciel.legacybridge.generated";


    private static final Jaxb2Marshaller marshaller = new Jaxb2Marshaller();

    @Autowired
    public ServiceParapheurProperties serviceParapheurProperties;

    public LegacyRetryHandler() {
        log.debug("LegacyRetryHandler - empty constructor");
        AutowiredAnnotationBeanPostProcessor bpp = new AutowiredAnnotationBeanPostProcessor();
        WebApplicationContext currentContext = WebApplicationContextLocator.getCurrentWebApplicationContext();
        bpp.setBeanFactory(currentContext.getAutowireCapableBeanFactory());
        bpp.processInjection(this);

        configureMarshaller();
    }

    @Override
    public boolean handleMessage(SOAPMessageContext context) {
        legacyRetryIfNeeded(context);
        return true;
    }

    @Override
    public boolean handleFault(SOAPMessageContext context) {
        return false;
    }

    @Override
    public void close(MessageContext context) {
    }

    @Override
    public Set<QName> getHeaders() {
        return new HashSet<>();
    }

    private void legacyRetryIfNeeded(SOAPMessageContext context) {
        Boolean isOutBound = (Boolean) context.get(MessageContext.MESSAGE_OUTBOUND_PROPERTY);
        QName operation = (QName) context.get(SOAPMessageContext.WSDL_OPERATION);

        if (!isRequestIsRetryable(operation)) {
            return;
        }

        if (!isOutBound) {
            this.parseInBoundRequestToContext(context);
        } else if (this.isFirstResponseOnError(context)) {
            this.retryRequestOnLegacyUrl(context, operation);
        }
    }

    private boolean isFirstResponseOnError(SOAPMessageContext context) {
        String response = this.getOutBoundFirstResponse(context);
        return StringUtils.containsAny(
                response.toLowerCase(),
                "inconnu dans le parapheur",
                "impossible de récupérer le dossier.",
                "impossible d'archiver le dossier."
        );
    }

    private boolean isRequestIsRetryable(QName operation) {
        boolean isUrlValid = StringUtils.isNotEmpty(this.getLegacyParapheurUrl());
        boolean isOperationRetryable = StringUtils.equalsAny(
                operation.getLocalPart().toLowerCase(),
                "getdossier",
                "archiverdossier",
                "gethistodossier",
                "echo"
        );

        return isUrlValid && isOperationRetryable;
    }

    private String getOutBoundFirstResponse(SOAPMessageContext context) {
        try (ByteOutputStream byteOutputStream = new ByteOutputStream()) {
            context.getMessage().writeTo(byteOutputStream);
            return new String(byteOutputStream.getBytes(), StandardCharsets.UTF_8);
        } catch (SOAPException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void parseInBoundRequestToContext(SOAPMessageContext context) {
        try (ByteOutputStream byteOutputStream = new ByteOutputStream()) {
            context.getMessage().writeTo(byteOutputStream);
            String request = new String(byteOutputStream.getBytes(), StandardCharsets.UTF_8);
            context.put(REQUEST_XML, request);
        } catch (SOAPException | IOException e) {
            throw new RuntimeException(e);
        }
    }

    private void retryRequestOnLegacyUrl(SOAPMessageContext context, QName operation) {
        String authHeader = ((Headers) context.get(SOAPMessageContext.HTTP_REQUEST_HEADERS))
                .get("authorization").get(0);

        Object response = this.requestLegacyParapheur(
                this.getLegacyParapheurUrl(),
                operation,
                (String) context.get(REQUEST_XML),
                authHeader
        );
        this.marshalResponseToBody(
                context,
                operation,
                response
        );
    }

    private void marshalResponseToBody(
            SOAPMessageContext context,
            QName operation,
            Object response) {
        try {
            JAXBElement element = new JAXBElement(
                    new QName(operation.getNamespaceURI(), operation.getLocalPart() + "Response"),
                    this.getClassFromOperation(operation),
                    response
            );
            context.getMessage().getSOAPBody().removeContents();

            Object objectToMarshal = this.getClassFromOperation(operation).equals(Object.class)
                    ? element.getValue()
                    : element;

            marshaller.createMarshaller().marshal(objectToMarshal, context.getMessage().getSOAPBody());
        } catch (JAXBException | SOAPException e) {
            log.debug("marshalResponseToBody | error while marshalling response : ", e);
            throw new RuntimeException(e);
        } catch (Exception e) {
            log.error("marshalResponseToBody | unknown exception : ", e);
            throw e;
        }
    }

    private Class getClassFromOperation(QName operation) {
        return switch (operation.getLocalPart().toLowerCase()) {
            case "getdossier" -> GetDossierResponse.class;
            case "archiverdossier" -> ArchiverDossierResponse.class;
            case "gethistodossier" -> GetHistoDossierResponse.class;
            default -> Object.class;
        };
    }

    private Object requestLegacyParapheur(String legacyParapheurUrl,
                                          QName operation,
                                          String request,
                                          String authHeader) {
        log.debug("requestLegacyParapheur - legacyParapheurUrl : {}", legacyParapheurUrl);

        try {
            XmlMapper xmlMapper = new XmlMapper();
            Map xmlParsedRequest = xmlMapper.readValue(request, Map.class);
            Map xmlParsedBody = getMap(xmlParsedRequest, "Body", emptyMap());

            Object data;
            if (operation.getLocalPart().equals("ArchiverDossier")) {
                Map requestData = getMap(xmlParsedBody, operation.getLocalPart() + "Request", emptyMap());

                ArchiverDossierRequest archiverDossierRequest = new ArchiverDossierRequest();
                archiverDossierRequest.setDossierID(getString(requestData, "DossierID", null));
                archiverDossierRequest.setArchivageAction(ArchivageAction.fromValue(
                        getString(requestData, "ArchivageAction", null)
                ));
                data = archiverDossierRequest;
            } else {
                data = new JAXBElement<>(
                        new QName(operation.getNamespaceURI(), operation.getLocalPart() + "Request"),
                        String.class,
                        getString(xmlParsedBody, operation.getLocalPart() + "Request", null)
                );
            }
            return getWebServiceTemplate()
                    .marshalSendAndReceive(
                            legacyParapheurUrl,
                            data,
                            message -> addAuthHeaderToMessage(message, authHeader)
                    );

        } catch (JsonProcessingException e) {
            log.error("requestLegacyParapheur | error while parsing the request body : {}", e.getMessage());
            log.debug("Exception detail : ", e);
            throw new RuntimeException(e);
        } catch (Exception e) {
            log.error("marshalResponseToBody | unknown exception : {}", e.getMessage());
            log.debug("Exception detail : ", e);
            throw e;
        }
    }

    private String getLegacyParapheurUrl() {
        return Optional.ofNullable(serviceParapheurProperties.getLegacyParapheurUrl())
                .filter(url -> !url.equals("${LEGACY_PARAPHEUR_URL}"))
                .orElse("");
    }

    private void configureMarshaller() {
        marshaller.setPackagesToScan(PACKAGES_TO_SCAN);
        marshaller.setMarshallerProperties(Map.of(Marshaller.JAXB_FORMATTED_OUTPUT, true));

        getWebServiceTemplate().setMarshaller(marshaller);
        getWebServiceTemplate().setUnmarshaller(marshaller);
    }

    private void addAuthHeaderToMessage(WebServiceMessage soapMessage, String authHeader) {
        MimeHeaders mimeHeader = ((SaajSoapMessage) soapMessage).getSaajMessage().getMimeHeaders();
        mimeHeader.setHeader("Authorization", authHeader);
        mimeHeader.setHeader("Content-Type", "text/xml;charset=utf-8");
    }

}
