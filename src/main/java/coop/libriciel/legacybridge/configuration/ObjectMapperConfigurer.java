/*
 * Legacy bridge
 * Copyright (C) 2020-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.legacybridge.configuration;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.JacksonException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.*;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.datatype.jsr310.JavaTimeModule;
import coop.libriciel.iparapheur.legacy.model.MediaType;
import lombok.extern.log4j.Log4j2;
import org.openapitools.jackson.nullable.JsonNullableModule;
import org.springframework.boot.autoconfigure.jackson.Jackson2ObjectMapperBuilderCustomizer;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import java.io.IOException;

@Configuration
@Log4j2
public class ObjectMapperConfigurer {



    private static final JsonDeserializer<MediaType> MEDIA_TYPE_DESERIALIZER = new JsonDeserializer<>() {
        @Override
        public MediaType deserialize(JsonParser p, DeserializationContext ctxt) throws IOException, JacksonException {

            String val  = p.getText();
            if (val == null) {
                return null;
            }

            String[] splittedMediatype = val.split("/");
            if (splittedMediatype.length < 2) {
                log.warn("invalid deserialized mediatype : {}", val);
                return null;
            }

            MediaType result = new MediaType();
            result.type(splittedMediatype[0]);
            result.subtype(splittedMediatype[1]);
            return result;
        }
    };


    @Bean
    public ObjectMapper modelMapper() {
        ObjectMapper mapper = new ObjectMapper();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        SimpleModule module = new SimpleModule();
        module.addDeserializer(MediaType.class, MEDIA_TYPE_DESERIALIZER);
        mapper.registerModule(module);

        mapper.registerModule(new JsonNullableModule());
        mapper.registerModule(new JavaTimeModule());
        return mapper;
    }

}
