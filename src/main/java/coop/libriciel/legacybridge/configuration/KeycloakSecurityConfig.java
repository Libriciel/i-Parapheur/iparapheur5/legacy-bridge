/*
 * Legacy bridge
 * Copyright (C) 2020-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.legacybridge.configuration;

import org.keycloak.adapters.springboot.KeycloakSpringBootProperties;
import org.keycloak.admin.client.KeycloakBuilder;
import org.keycloak.authorization.client.AuthzClient;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.builders.WebSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import static org.keycloak.OAuth2Constants.PASSWORD;

@Configuration
@EnableWebSecurity
public class KeycloakSecurityConfig extends WebSecurityConfigurerAdapter {


    private @Value("${keycloak.resource}") String clientId;
    private @Value("${keycloak.auth-server-url}") String authServer;
    private @Value("${keycloak.realm}") String realm;


    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http.authorizeRequests().antMatchers("/**").permitAll();
        http.headers().frameOptions().disable();
        http.csrf().disable();
    }

    @Override
    public void configure(WebSecurity web) {
        web.ignoring().antMatchers("/**");
    }


    @Bean
    public KeycloakBuilder builder() {
        return KeycloakBuilder.builder()
                .realm(realm)
                .serverUrl(authServer)
                .clientId(clientId)
                .grantType(PASSWORD);
    }

    @Bean
    public AuthzClient authzClient(KeycloakSpringBootProperties kcProperties) {

        org.keycloak.authorization.client.Configuration configuration = new org.keycloak.authorization.client.Configuration(
                kcProperties.getAuthServerUrl(),
                kcProperties.getRealm(),
                kcProperties.getResource(),
                kcProperties.getCredentials(),
                null
        );
        return AuthzClient.create(configuration);
    }

}
