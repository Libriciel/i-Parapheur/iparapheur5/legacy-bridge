/*
 * Legacy bridge
 * Copyright (C) 2020-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.legacybridge.services;

import com.fasterxml.jackson.databind.ObjectMapper;
import coop.libriciel.iparapheur.internal.client.AdminFolderApi;
import coop.libriciel.iparapheur.internal.model.ColumnedTaskListRequest;
import coop.libriciel.iparapheur.internal.model.PageFolderDto;
import coop.libriciel.iparapheur.internal.model.StepDefinitionDto;
import coop.libriciel.iparapheur.internal.model.WorkflowDefinitionDto;
import coop.libriciel.iparapheur.legacy.client.CurrentUserApi;
import coop.libriciel.iparapheur.legacy.client.MetadataApi;
import coop.libriciel.iparapheur.legacy.model.MetadataRepresentation;
import coop.libriciel.iparapheur.legacy.model.UserDto;
import coop.libriciel.iparapheur.legacy.model.UserRepresentation;
import coop.libriciel.iparapheur.legacy.model.Task;
import coop.libriciel.iparapheur.legacy.model.Action;
import coop.libriciel.iparapheur.legacy.model.DetachedSignature;
import coop.libriciel.iparapheur.legacy.model.State;
import coop.libriciel.iparapheur.provisioning.client.AdminDeskApi;
import coop.libriciel.iparapheur.provisioning.client.AdminTenantUserApi;
import coop.libriciel.iparapheur.provisioning.client.AdminTypologyApi;
import coop.libriciel.iparapheur.provisioning.model.DeskDto;
import coop.libriciel.iparapheur.provisioning.model.MetadataDto;
import coop.libriciel.iparapheur.provisioning.model.MetadataType;
import coop.libriciel.iparapheur.provisioning.model.SubtypeDto;
import coop.libriciel.iparapheur.provisioning.model.SubtypeMetadataDto;
import coop.libriciel.iparapheur.provisioning.model.TypeDto;
import coop.libriciel.iparapheur.standard.client.FolderApi;
import coop.libriciel.iparapheur.standard.client.SecureMailApi;
import coop.libriciel.iparapheur.standard.client.TypologyApi;
import coop.libriciel.iparapheur.standard.client.WorkflowApi;
import coop.libriciel.iparapheur.standard.model.MailParams;
import coop.libriciel.iparapheur.standard.model.SubtypeRepresentation;
import coop.libriciel.iparapheur.standard.model.TypeRepresentation;
import coop.libriciel.legacybridge.configuration.properties.ServiceParapheurProperties;
import coop.libriciel.legacybridge.generated.*;
import coop.libriciel.legacybridge.models.AuthData;
import coop.libriciel.legacybridge.models.exceptions.FolderRetrievalException;
import coop.libriciel.legacybridge.models.parapheur.CreateFolderRequest;
import coop.libriciel.legacybridge.models.parapheur.PageMetadataRepresentation;
import coop.libriciel.legacybridge.models.parapheur.PageSubtypeRepresentation;
import coop.libriciel.legacybridge.models.parapheur.PageTypeRepresentation;
import coop.libriciel.legacybridge.models.parapheur.PageUserRepresentation;
import coop.libriciel.legacybridge.models.parapheur.SimpleTaskParams;
import coop.libriciel.legacybridge.models.parapheur.*;
import coop.libriciel.legacybridge.utils.RequestUtils;
import coop.libriciel.legacybridge.utils.exceptions.IPInternalException;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.core.io.buffer.DataBuffer;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.http.HttpStatus;
import org.springframework.http.client.MultipartBodyBuilder;
import org.springframework.http.codec.json.Jackson2JsonDecoder;
import org.springframework.lang.Nullable;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.client.ExchangeStrategies;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.server.ResponseStatusException;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Flux;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.math.BigInteger;
import java.net.URI;
import java.util.*;
import java.util.stream.Collectors;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

import static coop.libriciel.iparapheur.legacy.model.Action.*;
import static coop.libriciel.iparapheur.legacy.model.FolderVisibility.CONFIDENTIAL;
import static coop.libriciel.iparapheur.legacy.model.FolderVisibility.PUBLIC;
import static coop.libriciel.iparapheur.legacy.model.State.*;
import static coop.libriciel.iparapheur.provisioning.model.SignatureProtocol.HELIOS;
import static coop.libriciel.legacybridge.configuration.WebClientConfigurer.BIG_WEBCLIENT_BUILDER_BEAN;
import static coop.libriciel.legacybridge.configuration.WebClientConfigurer.MEDIUM_WEBCLIENT_BUILDER_BEAN;
import static coop.libriciel.legacybridge.generated.ArchivageAction.ARCHIVER;
import static coop.libriciel.legacybridge.generated.ArchivageAction.EFFACER;
import static coop.libriciel.legacybridge.utils.MimeTypeUtils.APPLICATION_ZIP_VALUE;
import static coop.libriciel.legacybridge.utils.MimeTypeUtils.getExtensionFromMimeType;
import static coop.libriciel.legacybridge.utils.RequestUtils.*;
import static coop.libriciel.legacybridge.utils.StringUtils.RESERVED_PREFIX;
import static java.util.Collections.*;
import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.ObjectUtils.isNotEmpty;
import static org.apache.commons.lang3.StringUtils.EMPTY;
import static org.springframework.http.HttpHeaders.CONTENT_DISPOSITION;
import static org.springframework.http.HttpHeaders.CONTENT_TYPE;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.MediaType.*;


@Service
@Log4j2
public class IparapheurService implements IparapheurServiceInterface {


    public static final String HTTP = "http";
    public static final String API_V1 = "api/v1";
    public static final String STD_API_V1 = "api/standard/v1";
    public static final String BASE_URL = API_V1 + "/tenant";
    public static final String STD_BASE_URL = STD_API_V1 + "/tenant";
    public static final String DESK_PATH = "desk";
    public static final String DRAFT_PATH = "draft";
    public static final String FOLDER_PATH = "folder";
    public static final String HISTORY_TASKS = "historyTasks";
    public static final String TASK_PATH = "task";
    public static final String VISA_PATH = "visa";
    public static final String REJECT_PATH = "reject";
    public static final String DOCUMENT_PATH = "document";
    public static final String DETACHED_SIGNATURE_PATH = "detachedSignature";
    public static final String SEARCH_URL = API_V1 + "/search/folders";
    public static final String DEFAULT_PRINT_DOC_NAME = "iParapheur_impression_dossier.pdf";
    public static final String FOLDER_TITLE_METADATA_KEY = "ph:dossierTitre";

    public static final Set<Action> VALIDATED_ACTIONS = Set.of(VISA, SEAL, SIGNATURE, SECURE_MAIL, EXTERNAL_SIGNATURE, SECOND_OPINION);
    public static final int MAX_PAGE_SIZE = 200;

    private final AuthServiceInterface authService;
    private final ServiceParapheurProperties serviceParapheurProperties;
    private final ModelMapper modelMapper;
    private final ObjectMapper objectMapper;
    private final WebClient.Builder sharedSmallWebclientBuilder;
    private final WebClient.Builder sharedMediumWebclientBuilder;
    private final WebClient.Builder sharedBigWebClientBuilder;


    @Autowired
    public IparapheurService(AuthServiceInterface authService,
                             ObjectMapper objectMapper,
                             ServiceParapheurProperties serviceParapheurProperties,
                             WebClient.Builder sharedSmallWebclientBuilder,
                             @Qualifier(MEDIUM_WEBCLIENT_BUILDER_BEAN)
                             WebClient.Builder sharedMediumWebclientBuilder,
                             @Qualifier(BIG_WEBCLIENT_BUILDER_BEAN)
                             WebClient.Builder sharedBigWebClientBuilder) {
        this.objectMapper = objectMapper;
        this.authService = authService;
        this.serviceParapheurProperties = serviceParapheurProperties;
        this.sharedSmallWebclientBuilder = sharedSmallWebclientBuilder;
        this.sharedMediumWebclientBuilder = sharedMediumWebclientBuilder;
        this.sharedBigWebClientBuilder = sharedBigWebClientBuilder;
        this.modelMapper = new ModelMapper();
        modelMapper.getConfiguration().setAmbiguityIgnored(true);
        modelMapper.createTypeMap(Task.class, CustomTask.class).addMappings(m -> m.skip(CustomTask::setRead));
    }


    @Override
    public String echoRequest(String request) throws ResponseStatusException {
        log.info("Echo request...");
        AuthData authData = this.authService.login();

        if (authData.getAuthToken() == null) {
            throw new ResponseStatusException(INTERNAL_SERVER_ERROR);
        }

        UserDto user = this.findCurrentUser(authData);

        if (user == null) {
            log.warn("Error fetching current user.");
            return "Erreur d'authentification";
        }

        log.warn("Auth OK.");
        log.warn("[" + user.getUserName() + "] m'a dit: \"" + request + "\"!");
        return "[" + user.getUserName() + "] m'a dit: \"" + request + "\"!";
    }


    @Override
    public GetListeTypesResponse getTypeList() {
        log.info("Get type list request...");
        AuthData authData = this.authService.login();
        GetListeTypesResponse response = new GetListeTypesResponse();

        if (authData.isAuthenticatedWithOneDesk()) {
            List<TypeRepresentation> types = this.getCreationAllowedTypes(authData);
            types.forEach(type -> response.getTypeTechnique().add(type.getName()));
        }

        return response;
    }


    @Override
    public GetListeSousTypesResponse getSubtypeList(String parentTypeName) {
        log.info("Get subtype list request...");
        AuthData authData = this.authService.login();
        GetListeSousTypesResponse response = new GetListeSousTypesResponse();

        if (authData.isAuthenticatedWithOneDesk()) {
            log.warn("Auth OK.");
            TypeDto type = this.getType(parentTypeName, authData);

            if (type != null) {
                List<SubtypeRepresentation> subtypes = this.getCreationAllowedSubtypes(authData, type.getId());
                subtypes.stream()
                        .map(subtype -> this.getSubtypeById(type.getId(), subtype.getId(), authData))
                        .filter(subtype -> StringUtils.isEmpty(subtype.getCreationWorkflowId()))
                        .forEach(subtype -> response.getSousType().add(subtype.getName()));
            }
        }
        return response;
    }


    @Override
    public GetCircuitResponse getWorkflow(GetCircuitRequest request) {
        log.info("Get workflow request...");
        AuthData authData = this.authService.login();

        GetCircuitResponse response = new GetCircuitResponse();

        if (!authData.isAuthenticatedWithOneDesk()) {
            log.warn("Auth KO on getWorkflow.");
            return response;
        }

        SubtypeDto subtype = this.getSubtype(request.getTypeTechnique(), request.getSousType(), authData);

        if (subtype == null || subtype.getValidationWorkflowId() == null) {
            log.warn("Subtype or validationWorkflowId null.");
            return response;
        }

        WorkflowDefinitionDto workflowDefinition = this.getWorkflowDefinition(
                subtype.getValidationWorkflowId(),
                authData
        );

        if (workflowDefinition == null || workflowDefinition.getSteps() == null) {
            log.warn("Workflow definition null.");
            return response;
        }

        workflowDefinition.getSteps()
                .stream()
                .filter(Objects::nonNull)
                .forEach(step -> {
                    EtapeCircuit etapeCircuit = new EtapeCircuit();
                    etapeCircuit.setRole(RequestUtils.mapActionToRole(step.getType()));
                    etapeCircuit.setParapheur(RequestUtils.parseStepParapheur(step));
                    etapeCircuit.setPrenom("");
                    etapeCircuit.setNom(this.parseStepName(step, authData));
                    response.getEtapeCircuit().add(etapeCircuit);
                });


        Optional<EtapeCircuit> archivingStepOpt = response.getEtapeCircuit().stream()
                .filter(etape -> StringUtils.equals(etape.getRole(), ROLE_ARCHIVAGE))
                .findAny();

        if (archivingStepOpt.isEmpty()) {
            UserDto currentUser = this.findCurrentUser(authData);

            EtapeCircuit finalStep = new EtapeCircuit();
            finalStep.setParapheur(authData.getDesk().getName());
            finalStep.setPrenom("");

            if (currentUser != null) {
                finalStep.setNom(currentUser.getFirstName() + " " + currentUser.getLastName());
            }
            finalStep.setRole(ROLE_ARCHIVAGE);
            response.getEtapeCircuit().add(finalStep);
        }

        return response;
    }


    @Override
    public GetListeMetaDonneesResponse getMetadataList() {
        log.info("Get metadata list request...");
        AuthData authData = this.authService.login();

        GetListeMetaDonneesResponse response = new GetListeMetaDonneesResponse();

        if (!authData.isAuthenticatedWithOneDesk()) {
            log.warn("Auth KO on getMetadataList.");
            return response;
        }

        List<MetadataRepresentation> metadataList = this.getMetadata(authData);
        metadataList
                .stream()
                .filter(Objects::nonNull)
                .forEach(metadata -> {
                    MetaDonneeDefinition metaDonneeDefinition = new MetaDonneeDefinition();
                    ofNullable(metadata.getType())
                            .ifPresent(metadataType -> metaDonneeDefinition.setNature(
                                    mapNatureMetadata(metadata.getType()))
                            );
                    metaDonneeDefinition.setNomCourt(metadata.getKey());
                    metaDonneeDefinition.setNomLong(metadata.getName());
                    response.getMetaDonnee().add(metaDonneeDefinition);
                });

        return response;
    }


    @Override
    public IsUtilisateurExisteResponse doesUserExists(String username) {
        log.info("Does user exist request...");

        IsUtilisateurExisteResponse response = new IsUtilisateurExisteResponse();
        MessageRetour messageRetour = new MessageRetour();

        AuthData authData = this.authService.login();


        if (authData.getAuthToken() == null) {
            log.warn("Auth KO on doesUserExist.");
            messageRetour.setCodeRetour("KO");
            messageRetour.setSeverite("WARNING");
            messageRetour.setMessage("Authentification invalide.");
            response.setMessageRetour(messageRetour);
            return response;
        }

        Integer result = this.doesUserExistsRequest(username, authData);
        if (result == null) {
            log.warn("Error on user check : null result.");
            messageRetour.setCodeRetour("KO");
            messageRetour.setSeverite("INFO");
            messageRetour.setMessage("WebService non implémenté.");
        } else if (result.equals(0)) {
            log.warn("Unknown user username : {}.", username);
            messageRetour.setCodeRetour("KO");
            messageRetour.setSeverite("INFO");
            messageRetour.setMessage("Utilisateur inconnu.");
        } else {
            messageRetour.setCodeRetour("OK");
            messageRetour.setSeverite("INFO");
            messageRetour.setMessage(username + " existe.");
        }
        response.setMessageRetour(messageRetour);
        return response;
    }


    // TODO wait for users permissions
    @Override
    public GetListeSousTypesPourUtilisateurResponse getUserSubTypeList(GetListeSousTypesPourUtilisateurRequest request) {
        GetListeSousTypesPourUtilisateurResponse response = new GetListeSousTypesPourUtilisateurResponse();
        this.getSubtypeList(request.getTypeTechnique())
                .getSousType()
                .forEach(t -> response.getSousType().add(t));
        return response;
    }


    // TODO wait for users permissions
    @Override
    public GetListeTypesPourUtilisateurResponse getUserTypeList(String userId) {
        GetListeTypesPourUtilisateurResponse response = new GetListeTypesPourUtilisateurResponse();
        this.getTypeList()
                .getTypeTechnique()
                .forEach(t -> response.getTypeTechnique().add(t));
        return response;
    }


    // TODO wait for users permissions
    @Override
    public GetCircuitPourUtilisateurResponse getUserWorkflow(GetCircuitPourUtilisateurRequest request) {
        return null;
    }


    @Override
    public GetMetaDonneesRequisesPourTypeSoustypeResponse getRequiredMetadataForTypeAndSubtype(GetMetaDonneesRequisesPourTypeSoustypeRequest request) {
        log.info("Get required metadata for type and subtype request...");
        AuthData authData = this.authService.login();

        GetMetaDonneesRequisesPourTypeSoustypeResponse response = new GetMetaDonneesRequisesPourTypeSoustypeResponse();

        if (!authData.isAuthenticatedWithOneDesk()) {
            log.warn("Auth KO on getRequiredMetadataForTypeAndSubtype.");
            return response;
        }

        TypeDto type = this.getType(request.getTypeTechnique(), authData);
        SubtypeDto subtype = this.getSubtype(request.getTypeTechnique(), request.getSousType(), authData);

        if (type == null || subtype == null) {
            log.warn("Type or subtype null.");
            return response;
        }

        List<SubtypeMetadataDto> metadata = this.getMetadataByTypeAndSubtype(type.getId(), subtype.getId(), authData);
        metadata.forEach(subtypeMetadata -> {
            MetaDonneeDefinition metaDonneeDefinition = new MetaDonneeDefinition();
            metaDonneeDefinition.setNature(mapNatureMetadata(ofNullable(subtypeMetadata.getMetadata()).map(MetadataDto::getType).orElse(MetadataType.TEXT)));
            metaDonneeDefinition.setNomCourt(subtypeMetadata.getMetadata().getKey());
            metaDonneeDefinition.setNomLong(subtypeMetadata.getMetadata().getName());
            metaDonneeDefinition.setObligatoire(subtypeMetadata.isMandatory());
            ofNullable(subtypeMetadata.getMetadata().getRestrictedValues())
                    .orElse(emptyList())
                    .forEach(restrictedValue ->
                            metaDonneeDefinition
                                    .getValeurPossible()
                                    .add(restrictedValue)
                    );
            response.getMetaDonnee().add(metaDonneeDefinition);
        });


        return response;
    }


    @Override
    public CreerDossierResponse createDraft(CreerDossierRequest request) {
        log.info("Create draft folder request...");
        AuthData authData = this.authService.login();

        CreerDossierResponse response = new CreerDossierResponse();
        MessageRetour messageRetour = new MessageRetour();

        if (!authData.isAuthenticatedWithOneDesk()) {
            log.warn("Auth KO on createDraft.");
            RequestUtils.setMessageRetourToUnauthorized(messageRetour);
            response.setMessageRetour(messageRetour);
            return response;
        }

        Folder testFolder = this.getFolderByIdOrLegacyId(request.getDossierID(), authData);

        if (testFolder != null) {
            log.warn("The folder ID already exists : {}.", request.getDossierID());
            messageRetour.setCodeRetour("KO");
            messageRetour.setSeverite("ERROR");
            messageRetour.setMessage("Le nom de dossier est déjà présent dans le Parapheur: dossierID = " + ofNullable(request.getDossierID()).orElse(testFolder.getId()));
            response.setMessageRetour(messageRetour);
            return response;
        }

        TypeDto type = this.getType(request.getTypeTechnique(), authData);

        if (type == null) {
            log.warn("Cannot fetch type : {}, aborting folder creation.", request.getTypeTechnique());
            messageRetour.setCodeRetour("KO");
            messageRetour.setSeverite("ERROR");
            messageRetour.setMessage("Impossible de récupérer le type.");
            response.setMessageRetour(messageRetour);
            return response;
        }

        String testDocResult = RequestUtils.createFolderTests(request, type.getSignatureFormat());

        if (!testDocResult.equals("OK")) {
            log.warn("Folder tests KO, aborting folder creation.");
            log.debug("Folder tests result : {}", testDocResult);
            messageRetour.setCodeRetour("KO");
            messageRetour.setSeverite("ERROR");
            messageRetour.setMessage(testDocResult);
            response.setMessageRetour(messageRetour);
            return response;
        }

        SubtypeDto subtype = this.getSubtype(request.getTypeTechnique(), request.getSousType(), authData);
        if (subtype == null) {
            log.warn("Cannot fetch subtype : {}, aborting folder creation.", request.getSousType());
            messageRetour.setCodeRetour("KO");
            messageRetour.setSeverite("ERROR");
            messageRetour.setMessage("Impossible de récupérer le sous type.");
            response.setMessageRetour(messageRetour);
            return response;
        }

        if (StringUtils.isNotEmpty(subtype.getCreationWorkflowId())) {
            log.warn("This subtype contains a creation workflow : {}, aborting folder creation", request.getSousType());
            messageRetour.setCodeRetour("KO");
            messageRetour.setSeverite("ERROR");
            messageRetour.setMessage("Impossible d'utiliser une typologie contenant un circuit de création.");
            response.setMessageRetour(messageRetour);
            return response;
        }

        request.setTypeTechnique(type.getId());
        request.setSousType(subtype.getId());

        Folder folder = this.createFolder(request, authData);

        if (folder == null) {
            log.warn("An error happened while creating the draft.");
            messageRetour.setCodeRetour("KO");
            messageRetour.setSeverite("ERROR");
            messageRetour.setMessage("Impossible de créer le dossier.");
            response.setMessageRetour(messageRetour);
            return response;
        }

        boolean started = this.startFolder(
                folder.getId(),
                request.getAnnotationPublique(),
                request.getAnnotationPrivee(),
                authData
        );

        if (!started) {

            log.warn("Folder created but not started.");
            log.warn("We will try to delete the folder : {}", folder.getId());
            boolean deleted = this.deleteFolder(folder.getId(), authData);

            if (deleted) {
                log.info("The folder '{}' was deleted successfully", folder.getId());
            } else {
                log.warn("An error occurred while deleting the folder '{}'", folder.getId());
            }

            messageRetour.setCodeRetour("KO");
            messageRetour.setSeverite("ERROR");
            messageRetour.setMessage("Le dossier a été créé, mais une erreur est survenue lors du démarrage du circuit.");
            response.setMessageRetour(messageRetour);
            return response;
        }

        messageRetour.setCodeRetour("OK");
        messageRetour.setSeverite("INFO");

        if (request.getDossierTitre() != null) {
            messageRetour.setMessage("Dossier " + folder.getName() + " soumis dans le circuit");
        } else if (request.getDossierID() != null) {
            messageRetour.setMessage("Dossier " + request.getDossierID() + " soumis dans le circuit");
        }

        if (StringUtils.isEmpty(request.getDossierID())) {
            response.setDossierID(folder.getId());
        }

        response.setMessageRetour(messageRetour);
        return response;
    }


    private XMLGregorianCalendar getStepLogDate(Task step) {
        XMLGregorianCalendar logTimeStamp = null;
        if (step.getDate() != null) {
            logTimeStamp = getFolderLogTime(Date.from(step.getDate().toInstant()));
        }
        return logTimeStamp;
    }

    private XMLGregorianCalendar getStepBeginDate(Task step) {
        XMLGregorianCalendar logTimeStamp = null;
        if (step.getBeginDate() != null) {
            logTimeStamp = getFolderLogTime(Date.from(step.getBeginDate().toInstant()));
        }
        return logTimeStamp;
    }

    private XMLGregorianCalendar getFolderLogTime(Date beginDate) {
        XMLGregorianCalendar logTimeStamp = null;
        try {
            GregorianCalendar gregorianCalendar = new GregorianCalendar();
            gregorianCalendar.setTime(beginDate);
            logTimeStamp = DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar);
        } catch (DatatypeConfigurationException e) {
            // Do nothing
        }
        return logTimeStamp;
    }

    @Override
    public GetHistoDossierResponse getFolderHistory(String folderId) {
        log.warn("Get folder history request...");
        AuthData authData = this.authService.login();

        MessageRetour messageRetour = new MessageRetour();
        GetHistoDossierResponse response = new GetHistoDossierResponse();

        if (!authData.isAuthenticatedWithOneDesk()) {
            log.warn("Auth KO on getFolderHistory.");
            RequestUtils.setMessageRetourToUnauthorized(messageRetour);
            response.setMessageRetour(messageRetour);
            return response;
        }

        Folder folder = this.getFolderByIdOrLegacyId(folderId, authData);
        if (folder == null) {
            log.warn("Unknown folderId : {}.", folderId);
            messageRetour.setCodeRetour("KO");
            messageRetour.setSeverite("ERROR");
            messageRetour.setMessage("Le dossier '" + folderId + "' est inconnu dans le parapheur.");
            response.setMessageRetour(messageRetour);
            return response;
        }

        List<CustomTask> populatedTasks = this.getFolderHistoryTasks(folder.getId(), authData)
                .stream()
                .filter(task -> task.getWorkflowIndex() != null)
                .sorted(Comparator.comparing(Task::getWorkflowIndex))
                .map(t -> modelMapper.map(t, CustomTask.class))
                .toList();


        for (CustomTask step : populatedTasks) {
            if (step.getAction() != null && step.getAction().equals(SIGNATURE)) {
                populatedTasks
                        .stream()
                        .filter(t -> t.getWorkflowIndex() != null)
                        .filter(t -> t.getAction() != null)
                        .filter(t -> t.getStepIndex() != null)
                        .filter(t -> t.getWorkflowIndex().equals(step.getWorkflowIndex()))
                        .filter(t -> t.getStepIndex().equals(step.getStepIndex()))
                        .filter(t -> t.getAction().equals(READ))
                        .findFirst()
                        .ifPresent(task -> {
                            if (step.getUser() == null) {
                                step.setUser(task.getUser());
                            }
                            if (step.getDate() == null) {
                                step.setDate(task.getDate());
                            }
                            step.setRead(true);
                        });
            }
        }

        populatedTasks = populatedTasks
                .stream()
                .filter(step -> step.getAction() != READ)
                .filter(step -> step.getAction() != DELETE)
                .toList();

        int index = 0;

        for (CustomTask step : populatedTasks) {
            String userFirstName = ofNullable(step.getUser()).map(UserRepresentation::getFirstName).orElse(EMPTY);
            String userLastName = ofNullable(step.getUser()).map(UserRepresentation::getLastName).orElse(EMPTY);
            String folderLogName = userFirstName + " " + userLastName;

            Action action = step.getAction();
            State state = step.getState();

            List<LogDossier> responseLogs = response.getLogDossier();
            if (action == ARCHIVE) {
                LogDossier lastFolderLog = responseLogs
                        .stream()
                        .skip(responseLogs.size() - 1)
                        .findFirst()
                        .orElse(new LogDossier());

                LogDossier stepFolderLog = new LogDossier();
                stepFolderLog.setNom(lastFolderLog.getNom());
                stepFolderLog.setTimestamp(lastFolderLog.getTimestamp());
                stepFolderLog.setStatus("Archive");
                stepFolderLog.setAnnotation("Circuit terminé, dossier archivable");
                response.getLogDossier().add(stepFolderLog);
            }

            if (action != ARCHIVE && step.getBeginDate() != null) {

                log.debug("processing non-final history task with action: {}", step.getAction());

                boolean isStartAction = action == START;
                if (isStartAction) {
                    LogDossier startLogDossier = new LogDossier();
                    startLogDossier.setNom(folderLogName);
                    startLogDossier.setTimestamp(getStepLogDate(step));
                    startLogDossier.setStatus("NonLu");
                    startLogDossier.setAnnotation("Création de dossier");
                    responseLogs.add(startLogDossier);

                    LogDossier stepFolderLog = new LogDossier();
                    stepFolderLog.setNom(folderLogName);
                    stepFolderLog.setTimestamp(getStepLogDate(step));
                    stepFolderLog.setStatus("NonLu");
                    stepFolderLog.setAnnotation("Emission du dossier");
                    responseLogs.add(stepFolderLog);

                    if (index < populatedTasks.size() - 1) {
                        Task nextStep = populatedTasks.get(index + 1);
                        if (nextStep.getState() != PENDING) {
                            LogDossier currentDeskFolderLog = new LogDossier();
                            currentDeskFolderLog.setNom(folderLogName);
                            currentDeskFolderLog.setTimestamp(getStepLogDate(step));

                            if (nextStep.getAction() != null) {
                                currentDeskFolderLog.setStatus(getCurrentTaskStatus(nextStep.getAction()));
                            }
                            currentDeskFolderLog.setAnnotation(getCurrentTaskMessage(nextStep));
                            responseLogs.add(currentDeskFolderLog);
                        }
                    }
                }

                // Bonus step if READ or VALIDATED SIGNATURE
                boolean isSignature = action == SIGNATURE;
                boolean isNotRejected = step.getPerformedAction() != REJECT;
                boolean isReadOrValidated = step.isRead() || state == VALIDATED;
                if (isSignature && isNotRejected && isReadOrValidated) {
                    LogDossier readLogDossier = new LogDossier();
                    readLogDossier.setNom(folderLogName);
                    readLogDossier.setTimestamp(getStepLogDate(step));
                    readLogDossier.setStatus("Lu");
                    readLogDossier.setAnnotation("Dossier lu et prêt pour la signature");
                    responseLogs.add(readLogDossier);
                }

                boolean isAskSecondOpinion = step.getPerformedAction() == ASK_SECOND_OPINION;
                if (isAskSecondOpinion) {
                    LogDossier stepFolderLog = new LogDossier();
                    stepFolderLog.setNom(folderLogName);
                    stepFolderLog.setTimestamp(getStepLogDate(step));
                    stepFolderLog.setStatus("Vise");
                    stepFolderLog.setAnnotation("Demande d'avis complémentaire.");
                    responseLogs.add(stepFolderLog);
                }


                // Actual step
                boolean isNotPending = !(state == PENDING);

                if (isNotPending && !isStartAction && !isAskSecondOpinion && action != null) {

                    LogDossier stepFolderLog = new LogDossier();
                    stepFolderLog.setNom(folderLogName);
                    stepFolderLog.setTimestamp(getStepLogDate(step));
                    stepFolderLog.setStatus(mapActionToStatus(step));
                    if (step.getState() == PENDING) {
                        stepFolderLog.setAnnotation(getCurrentTaskMessage(step));
                    } else {
                        String stepAnnotation = shouldAnnotationBeOnSameStep(step)
                                ? step.getPublicAnnotation()
                                : RequestUtils.mapActionToAnnotation(action);
                        stepFolderLog.setAnnotation(stepAnnotation);
                    }
                    responseLogs.add(stepFolderLog);
                }

                // Bonus step for annotation.
                // Annotation step always come after the actual step
                if (step.getPublicAnnotation() != null && !shouldAnnotationBeOnSameStep(step)) {
                    LogDossier annotationLogDossier = new LogDossier();
                    annotationLogDossier.setNom(folderLogName);
                    annotationLogDossier.setTimestamp(getStepLogDate(step));
                    annotationLogDossier.setStatus(mapActionToStatus(step));
                    annotationLogDossier.setAnnotation(step.getPublicAnnotation());
                    responseLogs.add(annotationLogDossier);
                }

                // Future step and desk info
                if (step.getPerformedAction() != TRANSFER && index < populatedTasks.size() - 1 && !isStartAction && isNotRejected) {
                    Task nextStep = populatedTasks.get(index + 1);
                    if (VALIDATED_ACTIONS.contains(nextStep.getAction()) && nextStep.getState() != VALIDATED && nextStep.getPerformedAction() != ARCHIVE) {
                        LogDossier currentStepLogDossier = new LogDossier();
                        currentStepLogDossier.setNom(folderLogName);
                        currentStepLogDossier.setTimestamp(getStepBeginDate(nextStep));
                        if (nextStep.getAction() != null) {
                            currentStepLogDossier.setStatus(getCurrentTaskStatus(nextStep.getAction()));
                        }
                        currentStepLogDossier.setAnnotation(getCurrentTaskMessage(nextStep.getState() == TRANSFERRED ? step : nextStep));
                        responseLogs.add(currentStepLogDossier);
                    }
                }
            }
            index++;
        }


        messageRetour.setCodeRetour("OK");
        messageRetour.setMessage("");
        messageRetour.setSeverite("INFO");
        response.setMessageRetour(messageRetour);
        return response;
    }


    @Override
    public EffacerDossierRejeteResponse deleteRejectedFolder(String folderId) {
        log.info("Delete rejected folder request...");
        AuthData authData = this.authService.login();

        EffacerDossierRejeteResponse response = new EffacerDossierRejeteResponse();
        MessageRetour messageRetour = new MessageRetour();

        if (!authData.isAuthenticatedWithOneDesk()) {
            log.warn("Auth KO on deleteRejectedFolder.");
            RequestUtils.setMessageRetourToUnauthorized(messageRetour);
            response.setMessageRetour(messageRetour);
            return response;
        }

        Folder folder = this.getFolderByIdOrLegacyId(folderId, authData);

        if (folder == null || folder.getStepList()
                .stream()
                .map(Task::getState)
                .filter(Objects::nonNull)
                .noneMatch(state -> state.equals(REJECTED))) {
            log.warn("The folder does not exists or is not in the rejected state.");
            messageRetour.setCodeRetour("KO");
            messageRetour.setMessage("Le dossier renseigné n'est pas présent dans le parapheur ou n'est pas rejeté.");
            messageRetour.setSeverite("ERROR");
            response.setMessageRetour(messageRetour);
            return response;
        }

        boolean folderDeleted = this.deleteFolder(folder.getId(), authData);

        if (!folderDeleted) {
            log.warn("Unknown folderId : {}.", folderId);
            messageRetour.setCodeRetour("KO");
            messageRetour.setMessage("Le dossierID '" + folderId + "' est inconnu dans le parapheur.");
            messageRetour.setSeverite("ERROR");
            response.setMessageRetour(messageRetour);
            return response;
        }

        messageRetour.setCodeRetour("OK");
        messageRetour.setMessage("Dossier " + folderId + " supprimé du Parapheur.");
        messageRetour.setSeverite("INFO");
        response.setMessageRetour(messageRetour);
        return response;
    }


    @Override
    public RechercherDossiersResponse searchFolders(RechercherDossiersRequest request) {
        log.info("Search folders request...");
        AuthData authData = this.authService.login();

        log.info("searchFolders - request getDossierID : {}", request.getDossierID());
        log.info("searchFolders - request  getNombreDossiers: {}", request.getNombreDossiers());
        log.info("searchFolders - request getTypeTechnique : {}", request.getTypeTechnique());
        log.info("searchFolders - request  getSousType : {}", request.getSousType());
        log.info("searchFolders - request  getSousType : {}", request.getStatus());

        RechercherDossiersResponse response = new RechercherDossiersResponse();

        if (!authData.isAuthenticatedWithOneDesk()) {
            log.warn("searchFolders - aborting, no valid auth");
            return response;
        }

        List<String> folderIds = request.getDossierID();
        BigInteger folderCount = request.getNombreDossiers();
        String typeName = request.getTypeTechnique();
        String subtypeName = request.getSousType();
        String status = request.getStatus();
        String typeId = ofNullable(this.getType(typeName, authData))
                .map(TypeDto::getId)
                .orElse(null);
        String subtypeId = ofNullable(this.getSubtype(typeName, subtypeName, authData))
                .map(SubtypeDto::getId)
                .orElse(null);


        if ((StringUtils.isNotEmpty(request.getTypeTechnique()) && StringUtils.isEmpty(typeId))
                || (StringUtils.isNotEmpty(request.getSousType()) && StringUtils.isEmpty(subtypeId))) {
            log.warn("searchFolders - aborting, some empty params");
            return response;
        }


        List<Folder> populatedFolders = new ArrayList<>();


        if (!CollectionUtils.isEmpty(folderIds)
                && folderCount != null
                && folderIds.size() == folderCount.intValue()) {
            populatedFolders.addAll(this.listFoldersByFolderIds(folderIds, folderCount, authData));
        } else {


            String state = null;
            if (StringUtils.isNotEmpty(status)) {
                state = RequestUtils.mapStatus(status);
                if (StringUtils.isEmpty(state)) {
                    log.warn("searchFolders - aborting, mapped status is empty");
                    return response;
                }
            }

            // FIXME we should not search as admin, we should fix the visibility on folders
            List<coop.libriciel.iparapheur.internal.model.FolderDto> folderList = this.listFoldersAsAdmin(
                    authData,
                    typeId,
                    subtypeId,
                    state
            );

            for (coop.libriciel.iparapheur.internal.model.FolderDto value : folderList) {
                populatedFolders.add(this.getFolderById(value.getId(), authData));
            }
        }

        log.info("searchFolders - populated folder size :  {}", populatedFolders.size());

        RequestUtils.setSearchFoldersResponseData(populatedFolders, request, response, authData);

        return response;
    }


    @Override
    public ForcerEtapeResponse forceStep(ForcerEtapeRequest request) {
        log.info("Force step request...");
        log.debug("Force step - folderId  : {}, code transition (OK == visa): {}", request.getDossierID(), request.getCodeTransition());
        AuthData authData = this.authService.login();

        ForcerEtapeResponse response = new ForcerEtapeResponse();
        MessageRetour messageRetour = new MessageRetour();

        if (!authData.isAuthenticatedWithOneDesk()) {
            RequestUtils.setMessageRetourToUnauthorized(messageRetour);
            response.setMessageRetour(messageRetour);
            return response;
        }

        Folder folder = this.getFolderByIdOrLegacyId(request.getDossierID(), authData);
        if (folder == null) {
            log.warn("Unknown folderId : {}.", request.getDossierID());
            messageRetour.setCodeRetour("KO");
            messageRetour.setSeverite("FATAL");
            messageRetour.setMessage("Impossible de trouver le dossier.");
            response.setMessageRetour(messageRetour);
            return response;
        }

        String currentTaskId = this.getCurrentTaskId(request.getDossierID(), authData);
        if (currentTaskId == null) {
            log.warn("Cannot find the folder current task id.");
            messageRetour.setCodeRetour("KO");
            messageRetour.setSeverite("FATAL");
            messageRetour.setMessage("Impossible de trouver la tâche.");
            response.setMessageRetour(messageRetour);
            return response;
        }

        SimpleTaskParams simpleTaskParams = new SimpleTaskParams();
        simpleTaskParams.setPublicAnnotation(request.getAnnotationPublique());
        simpleTaskParams.setPrivateAnnotation(request.getAnnotationPrivee());

        HttpStatus httpStatus = this.executeActionOnFolder(
                folder.getId(),
                currentTaskId,
                simpleTaskParams,
                request.getCodeTransition(),
                authData
        );

        if (httpStatus == null || httpStatus.isError()) {
            log.warn("Error while bypassing the task.");
            messageRetour.setCodeRetour("KO");
            messageRetour.setSeverite("FATAL");
            messageRetour.setMessage("Impossible de forcer la tâche.");
            response.setMessageRetour(messageRetour);
            return response;
        }

        messageRetour.setCodeRetour("OK");
        messageRetour.setMessage("");
        messageRetour.setSeverite("INFO");
        response.setMessageRetour(messageRetour);
        return response;
    }


    @Override
    public GetDossierResponse getFolder(String folderId) {
        AuthData authData = this.authService.login();

        log.info("getFolder - folderId : {}", folderId);

        GetDossierResponse response = new GetDossierResponse();
        MessageRetour messageRetour = new MessageRetour();
        TypeMetaDonnees typeMetaDonnees = new TypeMetaDonnees();

        if (!authData.isAuthenticatedWithOneDesk()) {
            log.warn("Auth KO on getFolder.");
            RequestUtils.setMessageRetourToUnauthorized(messageRetour);
            response.setMessageRetour(messageRetour);
            return response;
        }

        Folder folder = this.getFolderByIdOrLegacyId(folderId, authData);

        response.setMetaDonnees(typeMetaDonnees);
        messageRetour.setCodeRetour("OK");
        messageRetour.setSeverite("INFO");
        messageRetour.setMessage("");

        try {
            if (folder == null) {
                throw new FolderRetrievalException("Impossible de récupérer le dossier.");
            }

            response.setDossierID(folderId);

            List<Task> validatedSteps = folder
                    .getStepList()
                    .stream()
                    .filter(step -> step.getState() != null)
                    .filter(step -> step.getState().equals(VALIDATED))
                    .toList();
            if (validatedSteps.size() > 1) {
                Task lastStep = validatedSteps.get(validatedSteps.size() - 1);
                response.setAnnotationPublique(StringUtils.isNotEmpty(lastStep.getPublicAnnotation()) ? lastStep.getPublicAnnotation() : "");
                response.setAnnotationPrivee(StringUtils.isNotEmpty(lastStep.getPrivateAnnotation()) ? lastStep.getPrivateAnnotation() : "");
            }
            response.setTypeTechnique(
                    ofNullable(folder.getType()).map(coop.libriciel.iparapheur.legacy.model.TypeDto::getName).orElse("(Type supprimé)")
            );
            response.setSousType(
                    ofNullable(folder.getSubtype()).map(coop.libriciel.iparapheur.legacy.model.SubtypeDto::getName).orElse("(Sous-type supprimé)")
            );

            if (folder.getVisibility() != null) {
                response.setVisibilite(
                        folder.getVisibility().equals(PUBLIC)
                                ? Visibilite.PUBLIC
                                : Visibilite.CONFIDENTIEL
                );
            } else {
                response.setVisibilite(Visibilite.CONFIDENTIEL);
            }
            response.setDateLimite(folder.getDueDate() != null ? folder.getDueDate().toString() : "");

            final String folderUid = folder.getId();

            List<Document> mainDocuments = folder
                    .getDocumentList()
                    .stream()
                    .filter(Document::getIsMainDocument)
                    .toList();


            TypeDoc legacyMainDoc = new TypeDoc();

            Document firstMainDocument = mainDocuments.stream().findFirst()
                    .orElseThrow(() -> new FolderRetrievalException("Le dossier récupéré n'a pas de document principal."));
            legacyMainDoc.setContentType(firstMainDocument.getMediaType().toString());


            TypeDoc legacyDetachedSignature = null;
            try {
                legacyDetachedSignature = retrieveDetachedSignaturesForDoc(authData, folderUid, firstMainDocument);
            } catch (IPInternalException e) {
                log.warn("failed  to retrieve detached signature for doc : {}", firstMainDocument.getName());
                messageRetour.setCodeRetour("KO");
                messageRetour.setSeverite("FATAL");
                messageRetour.setMessage("Impossible de récupérer les signatures détachées.");
            }

            if (legacyDetachedSignature != null) {
                response.setSignatureDocPrincipal(legacyDetachedSignature);
            }

            byte[] mainDocContent = this.getDocumentContent(folderUid, firstMainDocument.getId(), authData);
            if (mainDocContent == null) {
                throw new FolderRetrievalException("Impossible de récupérer le dossier.");
            }
            legacyMainDoc.setValue(mainDocContent);

            List<Document> annexes = folder
                    .getDocumentList()
                    .stream()
                    .filter(document -> !document.getIsMainDocument())
                    .toList();

            if (mainDocuments.size() > 1) {
                TypeDocAnnexes secondaryMainDocs = new TypeDocAnnexes();
                for (Document document : mainDocuments.subList(1, mainDocuments.size())) {
                    this.populateTypeDocs(document, secondaryMainDocs, folderUid, authData);
                }

                response.setDocumentsSupplementaires(secondaryMainDocs);
            }

            TypeDocAnnexes annexeDocuments = new TypeDocAnnexes();
            for (Document document : annexes) {
                this.populateTypeDocs(document, annexeDocuments, folderUid, authData);
            }
            DocAnnexe printDoc = this.retrievePrintDocForFolder(folderUid, authData);
            annexeDocuments.getDocAnnexe().add(printDoc);
            response.setDocumentsAnnexes(annexeDocuments);

            response.setDocPrincipal(legacyMainDoc);
            response.setNomDocPrincipal(mainDocuments.get(0).getName());


            TypeDto populatedType = this.getType(folder.getType().getName(), authData);
            if (populatedType != null && populatedType.getProtocol() == HELIOS) {
                response.setFichierPES(legacyMainDoc);
            }

            for (String key : folder.getMetadata().keySet()) {
                MetaDonnee metaDonnee = new MetaDonnee();
                metaDonnee.setNom(key);
                metaDonnee.setValeur(folder.getMetadata().get(key));
                typeMetaDonnees.getMetaDonnee().add(metaDonnee);
            }

            // add folder title as reserved metadata
            MetaDonnee titleMeta = new MetaDonnee();
            titleMeta.setNom(FOLDER_TITLE_METADATA_KEY);
            titleMeta.setValeur(folder.getName());
            typeMetaDonnees.getMetaDonnee().add(titleMeta);

        } catch (IOException e) {
            messageRetour.setCodeRetour("KO");
            messageRetour.setSeverite("FATAL");
            messageRetour.setMessage(e.getMessage());
        }


        response.setMessageRetour(messageRetour);
        return response;
    }


    @Override
    public ExercerDroitRemordDossierResponse exerciseFolderRemorseRight(String folderId) {
        AuthData authData = this.authService.login();
        MessageRetour messageRetour = new MessageRetour();
        ExercerDroitRemordDossierResponse response = new ExercerDroitRemordDossierResponse();

        if (!authData.isAuthenticatedWithOneDesk()) {
            log.warn("Auth KO on exerciseFolderRemorseRight.");
            RequestUtils.setMessageRetourToUnauthorized(messageRetour);
            response.setMessageRetour(messageRetour);
            return response;
        }

        Folder folder = this.getFolderByIdOrLegacyId(folderId, authData);

        Task task = folder.getStepList()
                .stream()
                .filter(step -> step.getState() != null)
                .filter(step -> step.getState().equals(State.CURRENT))
                .findFirst()
                .orElse(null);

        if (task == null) {
            messageRetour.setCodeRetour("KO");
            messageRetour.setMessage("");
            messageRetour.setSeverite("FATAL");
            response.setMessageRetour(messageRetour);
            return response;
        }

        if (isNotEmpty(task.getDesks())) {
            boolean isUndoOK = this.undo(task.getDesks().get(0).getId(), folderId, task.getId(), authData);

            if (!isUndoOK) {
                messageRetour.setCodeRetour("KO");
                messageRetour.setMessage("");
                messageRetour.setSeverite("FATAL");
                response.setMessageRetour(messageRetour);
                return response;
            }
        }


        messageRetour.setCodeRetour("OK");
        messageRetour.setMessage("Droits de remords exercés avec succès.");
        messageRetour.setSeverite("INFO");
        response.setMessageRetour(messageRetour);
        return response;
    }


    @Override
    public ArchiverDossierResponse archiveFolder(ArchiverDossierRequest request) {
        log.info("Archive folder request...");
        AuthData authData = this.authService.login();

        ArchiverDossierResponse response = new ArchiverDossierResponse();
        MessageRetour messageRetour = new MessageRetour();

        if (!authData.isAuthenticatedWithOneDesk()) {
            log.warn("Auth KO on archiveFolder.");
            RequestUtils.setMessageRetourToUnauthorized(messageRetour);
            response.setMessageRetour(messageRetour);
            return response;
        }

        boolean folderDeletedOrArchived = false;
        Folder folder = this.getFolderByIdOrLegacyId(request.getDossierID(), authData);
        if (folder != null) {
            if (request.getArchivageAction() == ARCHIVER) {
                folderDeletedOrArchived = this.archive(
                        folder.getId(),
                        this.getCurrentTaskId(folder.getId(), authData),
                        authData
                );
            } else if (request.getArchivageAction() == EFFACER) {
                folderDeletedOrArchived = this.deleteFolder(folder.getId(), authData);
            }
        }


        if (!folderDeletedOrArchived) {
            log.warn("Cannot archive the folder.");
            messageRetour.setCodeRetour("KO");
            messageRetour.setSeverite("FATAL");
            messageRetour.setMessage("Impossible d'archiver le dossier.");
            response.setMessageRetour(messageRetour);
            return response;
        }

        messageRetour.setCodeRetour("OK");
        messageRetour.setSeverite("INFO");
        messageRetour.setMessage("Dossier " + request.getDossierID() + (request.getArchivageAction().equals(ARCHIVER) ? " archivé." : " supprimé du Parapheur."));
        response.setMessageRetour(messageRetour);
        return response;
    }


    @Override
    public EnvoyerDossierMailSecuriseResponse sendFolderSecureMail(EnvoyerDossierMailSecuriseRequest request) {
        log.info("Send folder secure mail request...");
        AuthData authData = this.authService.login();

        EnvoyerDossierMailSecuriseResponse response = new EnvoyerDossierMailSecuriseResponse();
        MessageRetour messageRetour = new MessageRetour();

        if (!authData.isAuthenticatedWithOneDesk()) {
            log.warn("Auth KO on sendFolderSecureMail.");
            RequestUtils.setMessageRetourToUnauthorized(messageRetour);
            response.setMessageRetour(messageRetour);
            return response;
        }

        String currentTaskId = this.getCurrentTaskId(request.getDossier(), authData);
        HttpStatus httpStatus = null;
        if (currentTaskId != null) {
            httpStatus = this.sendSecureMail(request, currentTaskId, authData);
        }

        if (httpStatus == null || httpStatus.isError()) {
            log.warn("Error while sending secure mail.");
            messageRetour.setCodeRetour("KO");
            messageRetour.setSeverite("FATAL");
            messageRetour.setMessage("Impossible d'envoyer le mail sécurisé.");
            response.setMessageRetour(messageRetour);
            return response;
        }

        messageRetour.setCodeRetour("OK");
        messageRetour.setSeverite("INFO");
        messageRetour.setMessage("MAil sécurisé envoyé.");
        response.setMessageRetour(messageRetour);
        return response;
    }

    // DEPRECATED ENDPOINTS START


    @Override
    public EnvoyerDossierPESResponse sendPESFolder(String folderId) {
        EnvoyerDossierPESResponse response = new EnvoyerDossierPESResponse();
        MessageRetour messageRetour = new MessageRetour();
        messageRetour.setCodeRetour("KO");
        messageRetour.setSeverite("Error");
        messageRetour.setMessage("Requête dépréciée, Non merci.");
        response.setMessageRetour(messageRetour);
        return response;
    }


    @Override
    public CreerDossierPESResponse createPESFolder(CreerDossierPESRequest request) {
        CreerDossierPESResponse response = new CreerDossierPESResponse();
        MessageRetour messageRetour = new MessageRetour();
        messageRetour.setCodeRetour("KO");
        messageRetour.setSeverite("FATAL");
        messageRetour.setMessage("Requete incorrecte, utiliser CreerDossier.");
        response.setMessageRetour(messageRetour);
        return response;
    }


    @Override
    public EnvoyerDossierTdTResponse sendTdTFolder(String folderId) {
        EnvoyerDossierTdTResponse response = new EnvoyerDossierTdTResponse();
        MessageRetour messageRetour = new MessageRetour();
        messageRetour.setCodeRetour("KO");
        messageRetour.setSeverite("FATAL");
        messageRetour.setMessage("Requete dépréciée, non merci.");
        response.setMessageRetour(messageRetour);
        return response;
    }


    @Override
    public GetStatutTdTResponse getTdtStatus(String folderId) {
        GetStatutTdTResponse response = new GetStatutTdTResponse();
        MessageRetour messageRetour = new MessageRetour();
        messageRetour.setCodeRetour("KO");
        messageRetour.setSeverite("FATAL");
        messageRetour.setMessage("Requete dépréciée, non merci.");
        response.setMessageRetour(messageRetour);
        return response;
    }

    @Override
    public GetClassificationActesTdtResponse getActesClassification(GetClassificationActesTdtRequest request) {
        GetClassificationActesTdtResponse response = new GetClassificationActesTdtResponse();
        MessageRetour messageRetour = new MessageRetour();
        messageRetour.setCodeRetour("KO");
        messageRetour.setSeverite("ERROR");
        messageRetour.setMessage("Requete dépréciée, non merci.");
        response.setMessageRetour(messageRetour);
        return response;
    }

    // DEPRECATED ENDPOINTS END

    private boolean archive(String folderId, String taskId, AuthData authData) {
        try {
            new WorkflowApi(authData.getStandardDefaultClient())
                    .sendToTrashBin(authData.getTenantId(), authData.getDesk().getId(), folderId, taskId)
                    .block(REQUEST_SHORT_TIMEOUT);
            return true;
        } catch (Exception e) {
            log.error("archive error : folderId : {}", folderId, e);
            return false;
        }
    }


    private List<TypeRepresentation> getCreationAllowedTypes(AuthData authData) {
        return new TypologyApi(authData.getStandardDefaultClient())
                .listCreationAllowedTypesWithResponseSpec(authData.getTenantId(), authData.getDesk().getId(), 0, 999, null)
                .bodyToMono(PageTypeRepresentation.class)
                .blockOptional(REQUEST_LONG_TIMEOUT)
                .orElse(new PageTypeRepresentation())
                .getContent();
    }

    private List<SubtypeRepresentation> getCreationAllowedSubtypes(AuthData authData, String typeId) {
        return new TypologyApi(authData.getStandardDefaultClient())
                .listCreationAllowedSubtypesWithResponseSpec(authData.getTenantId(), authData.getDesk().getId(), typeId, 0, 999, null)
                .bodyToMono(PageSubtypeRepresentation.class)
                .blockOptional(REQUEST_LONG_TIMEOUT)
                .orElse(new PageSubtypeRepresentation())
                .getContent();
    }

    private TypeDto getType(String typeName, AuthData authData) {
        try {
            List<TypeRepresentation> types = this.getCreationAllowedTypes(authData);

            TypeRepresentation type = types.stream()
                    .filter(t -> t.getName().equals(typeName))
                    .findFirst()
                    .orElse(null);

            if (type != null) {
                return new AdminTypologyApi(authData.getProvisioningDefaultClient())
                        .getType(authData.getTenantId(), type.getId())
                        .blockOptional(REQUEST_SHORT_TIMEOUT)
                        .orElse(null);
            }
            return null;
        } catch (Exception e) {
            log.error("getType error : typeName : {}", typeName, e);
            return null;
        }
    }


    private SubtypeDto getSubtype(String typeName, String subtypeName, AuthData authData) {
        TypeDto type = this.getType(typeName, authData);

        if (type == null) return null;

        List<SubtypeRepresentation> subtypes = this.getCreationAllowedSubtypes(authData, type.getId());
        SubtypeRepresentation subtypeRepresentation = subtypes
                .stream()
                .filter(subtype -> subtype.getName().equals(subtypeName))
                .findFirst()
                .orElse(null);

        if (subtypeRepresentation == null) {
            return null;
        }

        return this.getSubtypeById(type.getId(), subtypeRepresentation.getId(), authData);
    }


    private SubtypeDto getSubtypeById(String typeId, String subtypeId, AuthData authData) {
        return new AdminTypologyApi(authData.getProvisioningDefaultClient())
                .getSubtype(authData.getTenantId(), typeId, subtypeId)
                .blockOptional(REQUEST_SHORT_TIMEOUT)
                .orElse(null);
    }

    private List<Folder> listFoldersByFolderIds(List<String> folderIds, BigInteger folderCount, AuthData authData) {
        List<Folder> results = new ArrayList<>();
        if (folderIds.size() != folderCount.intValue()) {
            return emptyList();
        }

        for (String id : folderIds) {
            Folder folder = this.getFolderByIdOrLegacyId(id, authData);

            if (folder != null) {
                results.add(folder);
            }
        }
        return results;
    }


    private String getCurrentTaskId(String folderId, AuthData authData) {
        Folder folder = this.getFolderByIdOrLegacyId(folderId, authData);

        if (folder == null) {
            return null;
        }

        return folder
                .getStepList()
                .stream()
                .filter(task -> task.getState() != null)
                .filter(task -> task.getState().equals(PENDING))
                .findFirst()
                .map(Task::getId)
                .orElse(null);
    }

    private MultipartBodyBuilder generateFolderBuilder(CreerDossierRequest request, AuthData authData) {
        if (authData.getDesk().getId() == null) {
            log.debug("auth error : Desk id is null");
            return null;
        }

        MultipartBodyBuilder builder = new MultipartBodyBuilder();
        builder.part("tenantId", authData.getTenantId());
        builder.part("deskId", authData.getDesk().getId());

        CreateFolderRequest draftFolderParams = new CreateFolderRequest();
        if (request.getDossierTitre() != null) {
            draftFolderParams.setName(request.getDossierTitre());
        } else if (request.getDossierID() != null) {
            draftFolderParams.setName(request.getDossierID());
        }

        draftFolderParams.setTypeId(request.getTypeTechnique());
        draftFolderParams.setSubtypeId(request.getSousType());
        draftFolderParams.setVisibility(
                request.getVisibilite().equals(Visibilite.PUBLIC)
                        ? PUBLIC
                        : CONFIDENTIAL
        );
        if (StringUtils.isNotEmpty(request.getDateLimite())) {
            draftFolderParams.setDueDate(request.getDateLimite());
        }

        if (request.getMetaData() != null) {
            List<MetadataRepresentation> iparapheurMetadataList = this.getMetadata(authData);
            Map<String, String> metadataMap = new HashMap<>();
            request.getMetaData()
                    .getMetaDonnee()
                    .forEach(metadata -> {
                        boolean isMetadataInSubtype = !iparapheurMetadataList
                                .stream()
                                .map(MetadataRepresentation::getKey)
                                .filter(name -> StringUtils.equals(name, metadata.getNom()))
                                .toList()
                                .isEmpty();
                        boolean isMetadataReserved = metadata.getNom().startsWith(RESERVED_PREFIX);

                        if (isMetadataInSubtype || isMetadataReserved) {
                            metadataMap.put(metadata.getNom(), metadata.getValeur());
                        }
                    });
            draftFolderParams.setMetadata(metadataMap);
        }


        if (StringUtils.isNotEmpty(request.getDossierID())) {
            draftFolderParams.setLegacyId(request.getDossierID());
        }


        TypeDoc mainDocument = request.getDocumentPrincipal();
        if (mainDocument != null) {
            String contentType = mainDocument.getContentType();
            String documentName = getDocumentNameWithExtension(
                    request.getNomDocPrincipal(),
                    "document-principal",
                    contentType,
                    -1
            );
            builder.part("mainFiles", new ByteArrayResource(mainDocument.getValue()))
                    .header(CONTENT_DISPOSITION, "form-data; name=mainFiles; filename=" + documentName)
                    .header(CONTENT_TYPE, contentType);


            if (request.getSignatureDocPrincipal() != null) {
                String detachedSignatureName = getDocumentNameWithExtension(
                        null,
                        "signature-detachee",
                        request.getSignatureDocPrincipal().getContentType(),
                        -1
                );
                builder.part("detachedSignatures", new ByteArrayResource(mainDocument.getValue()))
                        .header(
                                CONTENT_DISPOSITION,
                                "form-data; name=detachedSignatures; filename=" + detachedSignatureName
                        )
                        .header(CONTENT_TYPE, contentType);

                draftFolderParams.setDetachedSignaturesMapping(
                        singletonMap(documentName, singletonList(detachedSignatureName))
                );
            }


        }

        builder.part("createFolderRequest", draftFolderParams);

        addDocumentsToBuilder(request.getDocumentsSupplementaires(), "document-supplementaire-", "mainFiles", builder);
        addDocumentsToBuilder(request.getDocumentsAnnexes(), "document-annexe-", "annexeFiles", builder);


        return builder;
    }

    private void addDocumentsToBuilder(TypeDocAnnexes documentList, String fallBackName, String contentName, MultipartBodyBuilder builder) {
        if (documentList == null) return;

        List<DocAnnexe> documents = documentList.getDocAnnexe();
        for (int i = 0; i < documents.size(); i++) {
            DocAnnexe document = documents.get(i);
            String name = getDocumentNameWithExtension(document.getNom(), fallBackName, document.getMimetype(), i);
            builder.part(contentName, new ByteArrayResource(document.getFichier().getValue()))
                    .header(CONTENT_DISPOSITION, "form-data; name=" + contentName + "; filename=" + name)
                    .header(CONTENT_TYPE, document.getMimetype());
        }

    }

    private String getDocumentNameWithExtension(String name, String fallBackName, String contentType, int index) {
        String documentExtension = getExtensionFromMimeType(contentType);
        return ofNullable(name).orElse(fallBackName + (index == -1 ? "" : index) + "." + documentExtension);
    }

    private Folder getFolderByLegacyId(String folderId, AuthData authData) {
        try {
            Map<String, Object> queryParams = new HashMap<>();
            queryParams.put("legacyId", folderId);
            return this.getFolderById(
                    this.listFolders(authData, queryParams)
                            .stream()
                            .filter(folder -> folder.getLegacyId() != null)
                            .filter(folder -> !folder.getLegacyId().equals("null"))
                            .filter(folder -> folder.getLegacyId().equals(folderId))
                            .toList()
                            .stream()
                            .findFirst()
                            .map(Folder::getId)
                            .orElseThrow(() -> new IPInternalException("did not found folder by legacyId : " + folderId)),
                    authData
            );
        } catch (IPInternalException e) {
            log.debug("getFolderByLegacyId - no folder found with legacy ID '{}'", folderId);
            return null;
        } catch (Exception e) {
            log.warn("getFolderByLegacyId - got an unexpected error fetching the folder with legacyId : {} ", folderId, e);
            return null;
        }
    }


    private Folder getFolderByIdOrLegacyId(String folderId, AuthData authData) {
        Folder folder = this.getFolderById(folderId, authData);
        if (folder == null) {
            folder = this.getFolderByLegacyId(folderId, authData);
        }
        return folder;
    }


    private void populateTypeDocs(Document document, TypeDocAnnexes typeDocAnnexes, String folderId, AuthData authData) throws FolderRetrievalException {
        DocAnnexe docAnnexe = new DocAnnexe();
        docAnnexe.setNom(document.getName());
        TypeDoc typeDoc = new TypeDoc();
        if (document.getMediaType() != null) {
            typeDoc.setContentType(document.getMediaType().toString());
        } else if (document.getName().endsWith(".zip")) {
            typeDoc.setContentType("application/zip");
        } else {
            log.debug("Cannot parse Mediatype for document {}, setting default mimeType to pdf", document.getId());
            typeDoc.setContentType("application/pdf");
        }

        byte[] docContent = this.getDocumentContent(folderId, document.getId(), authData);
        if (docContent == null) {
            throw new FolderRetrievalException("Impossible de récupérer le dossier.");
        }

        typeDoc.setValue(docContent);
        docAnnexe.setFichier(typeDoc);


        TypeDoc legacyDetachedSignature = null;
        try {
            legacyDetachedSignature = retrieveDetachedSignaturesForDoc(authData, folderId, document);
        } catch (IPInternalException e) {
            log.warn("failed  to retrieve detached signature for additional doc : {}", document.getName());
        }

        if (legacyDetachedSignature != null) {
            docAnnexe.setSignature(legacyDetachedSignature);
        }

        typeDocAnnexes.getDocAnnexe().add(docAnnexe);
    }


    private DocAnnexe retrievePrintDocForFolder(String folderId, AuthData authData) throws FolderRetrievalException {
        DocAnnexe docAnnexe = new DocAnnexe();
        docAnnexe.setNom(DEFAULT_PRINT_DOC_NAME);
        TypeDoc typeDoc = new TypeDoc();
        typeDoc.setContentType(APPLICATION_PDF_VALUE);
        byte[] docContent = this.getPrintDocForFolder(folderId, authData);
        if (docContent == null) {
            throw new FolderRetrievalException("Impossible de récupérer le document d'impression.");
        }
        typeDoc.setValue(docContent);
        docAnnexe.setFichier(typeDoc);
        return docAnnexe;
    }


    private List<SubtypeMetadataDto> getMetadataByTypeAndSubtype(String typeId, String subtypeId, AuthData authData) {
        try {
            return new AdminTypologyApi(authData.getProvisioningDefaultClient())
                    .getSubtype(authData.getTenantId(), typeId, subtypeId)
                    .blockOptional(REQUEST_SHORT_TIMEOUT)
                    .map(subtype -> modelMapper.map(subtype, SubtypeDto.class))
                    .map(SubtypeDto::getSubtypeMetadataList)
                    .orElse(emptyList());
        } catch (Exception e) {
            log.error(
                    "getMetadataByTypeAndSubtype error : typeId : {}, subtypeId : {}",
                    typeId,
                    subtypeId,
                    e
            );
            return new ArrayList<>();
        }
    }


    private List<MetadataRepresentation> getMetadata(AuthData authData) {
        try {
            return new MetadataApi(authData.getLegacyDefaultClient())
                    .listMetadataWithResponseSpec(authData.getTenantId(), 0, 10000, List.of("NAME,ASC"))
                    .bodyToMono(PageMetadataRepresentation.class)
                    .blockOptional(REQUEST_SHORT_TIMEOUT)
                    .map(PageMetadataRepresentation::getContent)
                    .orElse(emptyList());
        } catch (Exception e) {
            log.error("getMetadata error :", e);
            return new ArrayList<>();
        }
    }


    private boolean startFolder(String folderId, String publicAnnotation, String privateAnnotation, AuthData authData) {
        coop.libriciel.iparapheur.standard.model.SimpleTaskParams simpleTaskParams = new coop.libriciel.iparapheur.standard.model.SimpleTaskParams();
        simpleTaskParams.setPublicAnnotation(publicAnnotation);
        simpleTaskParams.setPrivateAnnotation(privateAnnotation);

        try {
            new WorkflowApi(authData.getStandardDefaultClient())
                    .start(authData.getTenantId(), authData.getDesk().getId(), folderId, "taskId", simpleTaskParams)
                    .block(REQUEST_LONG_TIMEOUT);
            return true;
        } catch (Exception e) {
            log.error("startFolder error : folderId : {}", folderId, e);
            return false;
        }
    }


    private HttpStatus executeActionOnFolder(
            String folderId,
            String taskId,
            SimpleTaskParams simpleTaskParams,
            String transitionCode,
            AuthData authData) {

        String baseUrlPath;
        String actionPath;

        if (StringUtils.equals("OK", transitionCode)) {
            baseUrlPath = STD_BASE_URL;
            actionPath = VISA_PATH;
        } else {
            baseUrlPath = BASE_URL;
            actionPath = REJECT_PATH;
        }

        try {
            URI requestUri = UriComponentsBuilder.newInstance()
                    .scheme(HTTP).host(serviceParapheurProperties.getHost()).port(serviceParapheurProperties.getPort())
                    .pathSegment(baseUrlPath)
                    .pathSegment(authData.getTenantId())
                    .pathSegment(DESK_PATH)
                    .pathSegment(authData.getDesk().getId())
                    .pathSegment(FOLDER_PATH)
                    .pathSegment(folderId)
                    .pathSegment(TASK_PATH)
                    .pathSegment(taskId)
                    .pathSegment(actionPath)
                    .build().normalize().toUri();

            return WebClient.builder()
                    .build()
                    .put()
                    .uri(requestUri)
                    .contentType(APPLICATION_JSON)
                    .accept(APPLICATION_JSON)
                    .header("Authorization", authData.getAuthToken())
                    .body(BodyInserters.fromValue(simpleTaskParams))
                    .exchangeToMono(RequestUtils::getStatusFromEmptyBodyResponse)
                    .block(REQUEST_SHORT_TIMEOUT);
        } catch (Exception e) {
            log.error(
                    "executeActionOnFolder error : folderId : {}, taskId : {}, actionPath : {}",
                    folderId,
                    taskId,
                    transitionCode,
                    e
            );
            return null;
        }
    }


    private boolean deleteFolder(String folderId, AuthData authData) {
        try {
            new FolderApi(authData.getStandardDefaultClient())
                    .deleteFolder(authData.getTenantId(), authData.getDesk().getId(), folderId)
                    .block(REQUEST_LONG_TIMEOUT);
            return true;
        } catch (Exception e) {
            log.error("deleteFolder error : folderId : {}", folderId, e);
            return false;
        }
    }

    private List<coop.libriciel.iparapheur.internal.model.FolderDto> listFoldersAsAdmin(
            AuthData authData,
            @Nullable String typeId,
            @Nullable String subtypeId,
            @Nullable String state) {

        log.debug("listFoldersAsAdmin - typeId : {} - subtypeId : {} - state : {}",
                typeId,
                subtypeId,
                state
        );

        List<coop.libriciel.iparapheur.internal.model.FolderDto> result = new ArrayList<>();
        long totalFolders = Integer.MAX_VALUE;
        try {
            coop.libriciel.iparapheur.internal.model.State stateParam =
                    state != null
                            ? coop.libriciel.iparapheur.internal.model.State.valueOf(state)
                            : null;
            for (int currentPage = 0; currentPage * MAX_PAGE_SIZE <= totalFolders; currentPage++) {

                ColumnedTaskListRequest columnedTaskListRequest = new ColumnedTaskListRequest();
                columnedTaskListRequest.setTypeId(typeId);
                columnedTaskListRequest.setSubtypeId(subtypeId);
                columnedTaskListRequest.setState(stateParam);

                PageFolderDto intermediaryRes = new AdminFolderApi(authData.getInternalDefaultClient())
                        .listFoldersAsAdmin(
                                authData.getTenantId(),
                                columnedTaskListRequest,
                                currentPage,
                                null,
                                null
                        )
                        .blockOptional(REQUEST_LONG_TIMEOUT)
                        .orElse(new PageFolderDto());

                totalFolders = intermediaryRes.getTotalElements();
                result.addAll(intermediaryRes.getContent());
            }
        } catch (Exception e) {
            log.error("listFoldersAsAdmin error", e);
            return new ArrayList<>();
        }

        return result;
    }


    private List<Folder> listFolders(AuthData authData, Map<String, Object> params) {
        log.debug("listFolders - queryParams : {}", params);

        UriComponentsBuilder baseUriComponentsBuilder = UriComponentsBuilder.newInstance()
                .scheme(HTTP).host(serviceParapheurProperties.getHost()).port(serviceParapheurProperties.getPort())
                .pathSegment(SEARCH_URL)
                .queryParam("tenantId", authData.getTenantId())
                .queryParam("pageSize", MAX_PAGE_SIZE);

        for (String key : params.keySet()) baseUriComponentsBuilder.queryParam(key, params.get(key));

        List<Folder> result = new ArrayList<>();
        long totalFolders = Integer.MAX_VALUE;
        try {
            for (long currentPage = 0; currentPage * MAX_PAGE_SIZE <= totalFolders; currentPage++) {
                log.debug("listFolders - fetching page {} of folders", currentPage);
                UriComponentsBuilder pageUriComponentsBuilder = baseUriComponentsBuilder.cloneBuilder();
                pageUriComponentsBuilder.queryParam("page", currentPage);

                URI uri = pageUriComponentsBuilder.build().normalize().toUri();

                uri = new URI(encodeSpecificCharsForUri(uri));

                PaginatedList<FolderSearchResult> intermediaryRes = sharedMediumWebclientBuilder
                        .build()
                        .get()
                        .uri(uri)
                        .accept(APPLICATION_JSON)
                        .header("Authorization", authData.getAuthToken())
                        .retrieve()
                        .bodyToMono(new ParameterizedTypeReference<PaginatedList<FolderSearchResult>>() {
                        })
                        .blockOptional(REQUEST_LONG_TIMEOUT)
                        .orElse(new PaginatedList<>());

                totalFolders = intermediaryRes.getTotal();
                result.addAll(
                        intermediaryRes.getData()
                                .stream()
                                .map(FolderSearchResult::getFolder)
                                .toList()
                );
            }
        } catch (Exception e) {
            log.error("listFolders error", e);
            return new ArrayList<>();
        }

        return result;
    }

    private Folder createFolder(CreerDossierRequest request, AuthData authData) {
        MultipartBodyBuilder bodyBuilder = this.generateFolderBuilder(request, authData);

        if (bodyBuilder == null) return null;

        try {
            URI requestUri = UriComponentsBuilder.newInstance()
                    .scheme(HTTP).host(serviceParapheurProperties.getHost()).port(serviceParapheurProperties.getPort())
                    .pathSegment(BASE_URL)
                    .pathSegment(authData.getTenantId())
                    .pathSegment(DESK_PATH)
                    .pathSegment(authData.getDesk().getId())
                    .pathSegment(DRAFT_PATH)
                    .build().normalize().toUri();

            return sharedSmallWebclientBuilder
                    .build()
                    .post()
                    .uri(requestUri)
                    .contentType(MULTIPART_FORM_DATA)
                    .accept(APPLICATION_JSON)
                    .header("Authorization", authData.getAuthToken())
                    .body(BodyInserters.fromMultipartData(bodyBuilder.build()))
                    .retrieve()
                    .bodyToMono(Folder.class)
                    .blockOptional(REQUEST_LONG_TIMEOUT)
                    .orElse(null);
        } catch (Exception e) {
            log.warn("Error creating folder", e);
            return null;
        }
    }


    private List<Task> getFolderHistoryTasks(String folderId, AuthData authData) {
        log.debug("getFolderHistoryTasks : {}", folderId);
        try {
            URI requestUri = UriComponentsBuilder.newInstance()
                    .scheme(HTTP).host(serviceParapheurProperties.getHost()).port(serviceParapheurProperties.getPort())
                    .pathSegment(BASE_URL)
                    .pathSegment(authData.getTenantId())
                    .pathSegment(FOLDER_PATH)
                    .pathSegment(folderId)
                    .pathSegment(HISTORY_TASKS)
                    .build().normalize().toUri();
            return WebClient.builder()
                    .build()
                    .get()
                    .uri(requestUri)
                    .accept(APPLICATION_JSON)
                    .header("Authorization", authData.getAuthToken())
                    .retrieve()
                    .bodyToMono(new ParameterizedTypeReference<List<Task>>() {
                    })
                    .blockOptional(REQUEST_SHORT_TIMEOUT)
                    .orElse(null);
        } catch (Exception e) {
            log.debug("getFolderHistoryTasks - got an error fetching the tasks : ", e);
            return emptyList();
        }
    }


    private Folder getFolderById(String folderId, AuthData authData) {
        log.debug("getFolderById : {}", folderId);
        try {
            URI requestUri = UriComponentsBuilder.newInstance()
                    .scheme(HTTP).host(serviceParapheurProperties.getHost()).port(serviceParapheurProperties.getPort())
                    .pathSegment(BASE_URL)
                    .pathSegment(authData.getTenantId())
                    .pathSegment(FOLDER_PATH)
                    .pathSegment(folderId)
                    .build().normalize().toUri();
            return WebClient.builder()
                    .codecs(configurer -> configurer.defaultCodecs().jackson2JsonDecoder(new Jackson2JsonDecoder(objectMapper)))
                    .build()
                    .get()
                    .uri(requestUri)
                    .accept(APPLICATION_JSON)
                    .header("Authorization", authData.getAuthToken())
                    .retrieve()
                    .bodyToMono(Folder.class)
                    .blockOptional(REQUEST_SHORT_TIMEOUT)
                    .orElse(null);

        } catch (WebClientResponseException e) {
            if (e.getStatusCode() == HttpStatus.NOT_FOUND) {
                log.debug("getFolderById - no folder found with ID '{}'", folderId);
            } else {
                log.warn("getFolderById - got an unexpected error fetching the folder : ", e);
            }
            return null;
        } catch (Exception e) {
            log.warn("getFolderById - got an unexpected error fetching the folder : ", e);
            return null;
        }

    }

    private WebClient.Builder createBigWebClientBuilder() {

        ExchangeStrategies exchangeStrategies = ExchangeStrategies.builder()
                .codecs(configurer -> configurer.defaultCodecs().maxInMemorySize(1024 * 1024 * 224)).build();

        return WebClient.builder().exchangeStrategies(exchangeStrategies);
    }

    private byte[] getDocumentContent(String folderId, String documentId, AuthData authData) {
        try {
            URI requestUri = UriComponentsBuilder.newInstance()
                    .scheme(HTTP).host(serviceParapheurProperties.getHost()).port(serviceParapheurProperties.getPort())
                    .pathSegment(BASE_URL)
                    .pathSegment(authData.getTenantId())
                    .pathSegment(FOLDER_PATH)
                    .pathSegment(folderId)
                    .pathSegment(DOCUMENT_PATH)
                    .pathSegment(documentId)
                    .build().normalize().toUri();

            return sharedBigWebClientBuilder
                    .build()
                    .get()
                    .uri(requestUri)
                    .accept(APPLICATION_OCTET_STREAM)
                    .header("Authorization", authData.getAuthToken())
                    .retrieve()
                    .bodyToMono(byte[].class)
                    .blockOptional(REQUEST_LONG_TIMEOUT)
                    .orElse(null);
        } catch (Exception e) {
            log.error("Error while requesting document {} for folder {} : ", documentId, folderId, e);
            return null;
        }
    }


    private byte[] getDetachedSignatureContent(String folderId, String documentId, String detachedSignatureId, AuthData authData) {
        try {
            URI requestUri = UriComponentsBuilder.newInstance()
                    .scheme(HTTP).host(serviceParapheurProperties.getHost()).port(serviceParapheurProperties.getPort())
                    .pathSegment(BASE_URL)
                    .pathSegment(authData.getTenantId())
                    .pathSegment(FOLDER_PATH)
                    .pathSegment(folderId)
                    .pathSegment(DOCUMENT_PATH)
                    .pathSegment(documentId)
                    .pathSegment(DETACHED_SIGNATURE_PATH)
                    .pathSegment(detachedSignatureId)
                    .build().normalize().toUri();

            return sharedBigWebClientBuilder
                    .build()
                    .get()
                    .uri(requestUri)
                    .accept(APPLICATION_OCTET_STREAM)
                    .header("Authorization", authData.getAuthToken())
                    .retrieve()
                    .bodyToMono(byte[].class)
                    .blockOptional(REQUEST_LONG_TIMEOUT)
                    .orElse(null);
        } catch (Exception e) {
            log.error("Error while requesting document {} for folder {} : ", documentId, folderId, e);
            return null;
        }
    }


    private @Nullable byte[] getPrintDocForFolder(String folderId, AuthData authData) {
        log.debug("getPrintDocForFolder with id: {}", folderId);

        try {
            Flux<DataBuffer> dataFlux = new coop.libriciel.iparapheur.internal.client.FolderApi(authData.getInternalDefaultClient())
                    .printFolderWithResponseSpec(authData.getTenantId(), folderId, true, emptyList())
                    .bodyToFlux(DataBuffer.class);

            try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
                DataBufferUtils.write(dataFlux, baos)
                        .map(DataBufferUtils::release)
                        .then()
                        .block(REQUEST_LONG_TIMEOUT);

                return baos.toByteArray();
            }

        } catch (Exception e) {
            log.error("Error while requesting print document for folder {} : ", folderId, e);
            return null;
        }
    }


    private boolean undo(String deskId, String folderId, String taskId, AuthData authData) {
        try {
            new WorkflowApi(authData.getStandardDefaultClient())
                    .undo(authData.getTenantId(), authData.getDesk().getId(), folderId, taskId)
                    .block(REQUEST_SHORT_TIMEOUT);
            return true;
        } catch (Exception e) {
            log.error(
                    "undo error : deskId : {}, folderId : {}, taskId : {}",
                    deskId,
                    folderId,
                    taskId,
                    e
            );
            return false;
        }
    }


    private WorkflowDefinitionDto getWorkflowDefinition(String workflowId, AuthData authData) {
        try {
            return new coop.libriciel.iparapheur.internal.client.WorkflowApi(authData.getInternalDefaultClient())
                    .getWorkflowDefinition(authData.getTenantId(), authData.getDesk().getId(), workflowId)
                    .blockOptional(REQUEST_SHORT_TIMEOUT)
                    .orElse(null);
        } catch (Exception e) {
            log.error("getWorkflowDefinition error : workflowId : {}", workflowId, e);
            return null;
        }
    }


    private Integer doesUserExistsRequest(String username, AuthData authData) {
        try {
            PageUserRepresentation result = new AdminTenantUserApi(authData.getProvisioningDefaultClient())
                    .listTenantUsersWithResponseSpec(
                            authData.getTenantId(),
                            0,
                            1000,
                            emptyList(),
                            username,
                            null
                    )
                    .bodyToMono(PageUserRepresentation.class)
                    .blockOptional(REQUEST_SHORT_TIMEOUT)
                    .orElse(null);

            if (result == null || result.getContent() == null) {
                return 0;
            }

            return result
                    .getContent()
                    .stream()
                    .anyMatch(user -> user.getUserName().equals(username))
                    ? 1
                    : 0;
        } catch (Exception e) {
            log.error("doesUserExistsRequest error : userName : {}", username, e);
            return null;
        }
    }


    private @Nullable UserDto findCurrentUser(AuthData authData) {
        try {
            return new CurrentUserApi(authData.getLegacyDefaultClient())
                    .getCurrentUser()
                    .blockOptional(REQUEST_SHORT_TIMEOUT)
                    .orElse(null);
        } catch (Exception e) {
            log.error("findCurrentUser error", e);
            return null;
        }
    }


    private List<UserRepresentation> listUserFromDesks(String deskId, AuthData authData) {
        try {
            return new AdminDeskApi(authData.getProvisioningDefaultClient())
                    .getDeskAsAdmin(authData.getTenantId(), deskId)
                    .blockOptional(REQUEST_SHORT_TIMEOUT)
                    .map(DeskDto::getOwners)
                    .map(users -> users.stream().map(user -> modelMapper.map(user, UserRepresentation.class)).toList())
                    .orElse(null);
        } catch (Exception e) {
            log.error("listUserFromDesks error : deskId : {}", deskId, e);
            return null;
        }
    }


    private HttpStatus sendSecureMail(EnvoyerDossierMailSecuriseRequest request, String taskId, AuthData authData) {

        MailParams mailParams = new MailParams();
        mailParams.setObject(request.getSubject());
        mailParams.setTo(Collections.singletonList(request.getRcptTo()));
        mailParams.setPassword(request.getPassword());
        mailParams.setMessage(request.getMessage());

        try {
            new SecureMailApi(authData.getStandardDefaultClient())
                    .requestSecureMail(authData.getTenantId(), authData.getDesk().getId(), request.getDossier(), taskId, mailParams)
                    .block(REQUEST_LONG_TIMEOUT);

            return HttpStatus.OK;

        } catch (Exception e) {
            log.error("sendSecureMail error : taskId : {}", taskId, e);
            return null;
        }
    }


    private String parseStepName(StepDefinitionDto step, AuthData authData) {
        List<UserRepresentation> deskUsers = new ArrayList<>();
        step.getValidatingDesks().forEach(d -> {
            List<UserRepresentation> usersFromDesk = this.listUserFromDesks(d.getId(), authData);
            deskUsers.addAll(ofNullable(usersFromDesk).orElse(emptyList()));
        });

        return deskUsers.stream()
                // warning is not actually true, technical users have null values there
                .filter(user -> user.getFirstName() != null && user.getLastName() != null)
                .map(IparapheurService::getUsersFullName)
                .collect(Collectors.joining(", "));
    }

    @NotNull
    private static String getUsersFullName(UserRepresentation user) {
        return user.getFirstName() + " " + user.getLastName();
    }


    public byte[] zipDetachedSignatures(List<DetachedSignature> detachedSignaturesData, List<byte[]> detachedSignatureContentList) {


        try (ByteArrayOutputStream baos = new ByteArrayOutputStream();
             ZipOutputStream zos = new ZipOutputStream(baos)) {
            for (int i = 0; i < detachedSignatureContentList.size(); i++) {
                byte[] detachedSignatureContent = detachedSignatureContentList.get(i);
                String defaultName = "detached-signature-%d".formatted(i);
                ZipEntry entry = new ZipEntry(
                        ofNullable(detachedSignaturesData.get(i)).map(DetachedSignature::getName).orElse(defaultName)
                );
                entry.setSize(detachedSignatureContent.length);
                zos.putNextEntry(entry);
                zos.write(detachedSignatureContent);
                zos.closeEntry();
            }
            zos.finish();
            return baos.toByteArray();
        } catch (IOException e) {
            log.error(
                    "zipDetachedSignature error : detachedSignatureIds : {}",
                    detachedSignaturesData.stream().map(DetachedSignature::getId).toList().toString(),
                    e
            );
            return null;
        }
    }


    private TypeDoc retrieveDetachedSignaturesForDoc(AuthData authData, String folderId, Document doc) {
        List<DetachedSignature> detachedSignatureData = doc.getDetachedSignatures();

        if (!CollectionUtils.isEmpty(detachedSignatureData)) {
            List<byte[]> detachedSignatureContents = detachedSignatureData
                    .stream()
                    .map(detachedSignature -> getDetachedSignatureContent(
                            folderId,
                            doc.getId(),
                            detachedSignature.getId(),
                            authData)
                    )
                    .filter(Objects::nonNull)
                    .toList();

            TypeDoc legacyDetachedSignature = new TypeDoc();
            legacyDetachedSignature.setContentType(APPLICATION_ZIP_VALUE);

            byte[] zipDetachedSignatureContent = this.zipDetachedSignatures(
                    detachedSignatureData,
                    detachedSignatureContents
            );

            if (zipDetachedSignatureContent == null) {
                throw new IPInternalException("Detached signature error");
            }

            legacyDetachedSignature.setValue(zipDetachedSignatureContent);
            return legacyDetachedSignature;
        }
        return null;
    }

    /**
     * @param step A task step containing a public annotation
     * @return true if the step is a VISA, a SEAL or a rejected SIGNATURE,
     * meaning the annotation will be place on a unique step.
     * false otherwise, meaning the annotation in the folder history will be placed on another step after this one.
     */
    private static boolean shouldAnnotationBeOnSameStep(Task step) {
        return step.getAction() != null
                && (step.getAction().equals(VISA) || step.getAction().equals(SEAL) || step.getAction().equals(SIGNATURE));
    }

}
