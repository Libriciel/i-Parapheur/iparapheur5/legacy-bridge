/*
 * Legacy bridge
 * Copyright (C) 2020-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.legacybridge.services;

import coop.libriciel.iparapheur.legacy.ApiClient;
import coop.libriciel.iparapheur.legacy.client.CurrentUserApi;
import coop.libriciel.iparapheur.legacy.client.TenantApi;
import coop.libriciel.iparapheur.legacy.model.DeskRepresentation;
import coop.libriciel.iparapheur.legacy.model.TenantRepresentation;
import coop.libriciel.legacybridge.configuration.properties.ServiceAuthProperties;
import coop.libriciel.legacybridge.configuration.properties.ServiceParapheurProperties;
import coop.libriciel.legacybridge.models.AuthData;
import coop.libriciel.legacybridge.models.exceptions.LocalizedStatusException;
import coop.libriciel.legacybridge.models.parapheur.PageDeskRepresentation;
import coop.libriciel.legacybridge.models.parapheur.PageTenantRepresentation;
import lombok.extern.log4j.Log4j2;
import org.keycloak.authorization.client.AuthzClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.reactive.function.client.WebClientResponseException;
import org.springframework.web.util.UriComponentsBuilder;

import java.net.URI;
import java.util.*;

import static coop.libriciel.legacybridge.configuration.WebClientConfigurer.MEDIUM_WEBCLIENT_BUILDER_BEAN;
import static coop.libriciel.legacybridge.utils.RequestUtils.REQUEST_SHORT_TIMEOUT;
import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Collections.emptyList;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.removeEnd;
import static org.springframework.http.HttpStatus.BAD_REQUEST;


@Service
@Log4j2
public class AuthService implements AuthServiceInterface {


    private final AuthzClient authzClient;
    private final ServiceParapheurProperties serviceParapheurProperties;
    private final ServiceAuthProperties serviceAuthProperties;

    private final WebClient.Builder sharedMediumWebclientBuilder;


    @Autowired
    public AuthService(AuthzClient authzClient,
                       ServiceAuthProperties authProperties,
                       ServiceParapheurProperties parapheurProperties,
                       @Qualifier(MEDIUM_WEBCLIENT_BUILDER_BEAN)
                       WebClient.Builder sharedMediumWebclientBuilder) {
        this.authzClient = authzClient;
        this.serviceAuthProperties = authProperties;
        this.serviceParapheurProperties = parapheurProperties;
        this.sharedMediumWebclientBuilder = sharedMediumWebclientBuilder;
    }



    private String createBearerToken(String rawToken) {
        return "Bearer " + rawToken;
    }


    private String getHttpAuthHeaderValue() {
        return Optional.ofNullable(((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()))
                .map(ServletRequestAttributes::getRequest)
                .map(httpServletRequest -> httpServletRequest.getHeader("Authorization"))
                .orElse(null);
    }


    private String getUserLogin() {
        String authHeader = getHttpAuthHeaderValue();
        if (authHeader != null && authHeader.startsWith("Basic ")) {
            while (authHeader.endsWith("=")) {
                authHeader = removeEnd(authHeader, "=");
            }
            byte[] decodedString = Base64.getDecoder().decode(authHeader.substring(6));
            String[] authData = new String(decodedString, UTF_8).split(":", 2);

            if (authData.length == 2) {
                log.debug("getUserLogin - logging in as {}", authData[0]);
                return authData[0];
            }
        }

        log.debug("getUserLogin - failed");
        return null;
    }


    private String getUserPassword() {
        log.debug("getUserPassword ");
        String authHeader = getHttpAuthHeaderValue();
        if (authHeader != null && authHeader.startsWith("Basic ")) {
            while (authHeader.endsWith("=")) {
                authHeader = removeEnd(authHeader, "=");
            }
            String[] authData = new String(Base64.getDecoder().decode(authHeader.substring(6)))
                    // TODO use a regex
                    .split(":", 2);

            if (authData.length == 2) {
                return authData[1];
            }
        }
        return null;
    }


    public AuthData login() {
        AuthData authData = new AuthData();

        try {
            String unwrappedToken = this.authzClient
                    .obtainAccessToken(this.getUserLogin(), this.getUserPassword())
                    .getToken();

            String unwrappedAdminToken = this.authzClient
                    .obtainAccessToken(serviceAuthProperties.getLogin(), serviceAuthProperties.getPassword())
                    .getToken();

            URI requestUri = UriComponentsBuilder.newInstance()
                    .scheme(serviceParapheurProperties.getProtocol())
                    .host(serviceParapheurProperties.getUrl())
                    .build().normalize().toUri();

            WebClient apiInternalWebClient = sharedMediumWebclientBuilder.build();

            coop.libriciel.iparapheur.internal.ApiClient internalDefaultClient = new coop.libriciel.iparapheur.internal.ApiClient(apiInternalWebClient);
            internalDefaultClient.setBasePath(requestUri.toString());
            internalDefaultClient.setAccessToken(unwrappedAdminToken);

            authData.setInternalDefaultClient(internalDefaultClient);

            coop.libriciel.iparapheur.provisioning.ApiClient provisioningDefaultClient = new coop.libriciel.iparapheur.provisioning.ApiClient();
            provisioningDefaultClient.setBasePath(requestUri.toString());
            provisioningDefaultClient.setAccessToken(unwrappedAdminToken);

            authData.setProvisioningDefaultClient(provisioningDefaultClient);

            coop.libriciel.iparapheur.legacy.ApiClient legacyDefaultClient = new coop.libriciel.iparapheur.legacy.ApiClient();
            legacyDefaultClient.setBasePath(requestUri.toString());
            legacyDefaultClient.setAccessToken(unwrappedToken);

            authData.setLegacyDefaultClient(legacyDefaultClient);

            coop.libriciel.iparapheur.standard.ApiClient standardDefaultClient = new coop.libriciel.iparapheur.standard.ApiClient();
            standardDefaultClient.setBasePath(requestUri.toString());
            standardDefaultClient.setAccessToken(unwrappedToken);

            authData.setStandardDefaultClient(standardDefaultClient);

            // Request

            String authToken = createBearerToken(unwrappedToken);
            String adminAuthToken = createBearerToken(unwrappedAdminToken);
            String tenantId = this.loadTenant(legacyDefaultClient);

            if (isEmpty(tenantId)) {
                authData.setAuthenticated(false);
                return authData;
            }

            authData.setAuthenticated(true);
            authData.setAuthToken(authToken);
            authData.setAdminAuthToken(adminAuthToken);
            authData.setTenantId(tenantId);
            DeskRepresentation desk = this.loadDesk(tenantId, legacyDefaultClient);
            authData.setDesk(desk);
        } catch (Exception e) {
            log.error("An error occurred during authentication : {}", e.getMessage());
            log.debug("error detail : ", e);
            authData.setAuthenticated(false);
            return authData;
        }

        return authData;
    }


    private String loadTenant(ApiClient apiClient) {

        log.info("Loading entities...");
        List<TenantRepresentation> tenants = this.listTenantsForCurrentUser(apiClient);

        if (tenants.isEmpty()) {
            log.error("No entity found for user, auth invalid");
            return null;
        } else if (tenants.size() > 1) {
            log.error("More than one entity found for user, auth invalid");
            return null;
        }

        return tenants.get(0).getId();
    }


    private DeskRepresentation loadDesk(String tenantId, ApiClient apiClient) {
        List<DeskRepresentation> desks = this.listUserDesks(apiClient);

        if (desks == null || desks.size() != 1) {
            log.error("Current user have zero or more than one associated desk, you can only have one.");
            return null;
        }

        log.info("Loading complete, entityId : " + tenantId + ", deskId : " + desks.get(0).getId());
        return desks.get(0);
    }


    private List<TenantRepresentation> listTenantsForCurrentUser(ApiClient apiClient) {
        try {
            return new TenantApi(apiClient)
                    .listTenantsForUserWithResponseSpec(0, 10000, List.of("NAME,ASC"), false)
                    .bodyToMono(PageTenantRepresentation.class)
                    .blockOptional(REQUEST_SHORT_TIMEOUT)
                    .map(PageTenantRepresentation::getContent)
                    .orElse(emptyList());
        } catch (WebClientResponseException e) {
            log.warn("Error on listTenantsForCurrentUser request", e);
            return new ArrayList<>();
        }
    }


    private List<DeskRepresentation> listUserDesks(ApiClient apiClient) {
        try {

            return new CurrentUserApi(apiClient)
                    .getDesksWithResponseSpec(null, 0, 2, List.of("NAME,ASC"))
                    .bodyToMono(PageDeskRepresentation.class)
                    .map(PageDeskRepresentation::getContent)
                    .blockOptional(REQUEST_SHORT_TIMEOUT)
                    .orElse(emptyList());
        } catch (WebClientResponseException e) {
            log.warn("Error on listTenantsForCurrentUser request", e);
            return new ArrayList<>();
        }
    }

}
