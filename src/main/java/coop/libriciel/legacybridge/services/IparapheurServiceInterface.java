/*
 * Legacy bridge
 * Copyright (C) 2020-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.legacybridge.services;

import coop.libriciel.legacybridge.generated.*;
import org.springframework.web.server.ResponseStatusException;

public interface IparapheurServiceInterface {
    String echoRequest(String request) throws ResponseStatusException;

    GetListeTypesResponse getTypeList();

    GetListeSousTypesResponse getSubtypeList(String parentTypeName);

    GetCircuitResponse getWorkflow(GetCircuitRequest request);

    GetListeMetaDonneesResponse getMetadataList();

    IsUtilisateurExisteResponse doesUserExists(String userId);

    GetListeSousTypesPourUtilisateurResponse getUserSubTypeList(GetListeSousTypesPourUtilisateurRequest request);

    GetListeTypesPourUtilisateurResponse getUserTypeList(String userId);

    GetCircuitPourUtilisateurResponse getUserWorkflow(GetCircuitPourUtilisateurRequest request);

    GetMetaDonneesRequisesPourTypeSoustypeResponse getRequiredMetadataForTypeAndSubtype(GetMetaDonneesRequisesPourTypeSoustypeRequest request);

    CreerDossierResponse createDraft(CreerDossierRequest request);

    GetHistoDossierResponse getFolderHistory(String folderId);

    EffacerDossierRejeteResponse deleteRejectedFolder(String folderId);

    RechercherDossiersResponse searchFolders(RechercherDossiersRequest request);

    ForcerEtapeResponse forceStep(ForcerEtapeRequest request);

    GetDossierResponse getFolder(String folderId);

    ExercerDroitRemordDossierResponse exerciseFolderRemorseRight(String folderId);

    ArchiverDossierResponse archiveFolder(ArchiverDossierRequest request);

    EnvoyerDossierMailSecuriseResponse sendFolderSecureMail(EnvoyerDossierMailSecuriseRequest request);

    // DEPRECATED
    EnvoyerDossierPESResponse sendPESFolder(String folderId);

    CreerDossierPESResponse createPESFolder(CreerDossierPESRequest request);

    EnvoyerDossierTdTResponse sendTdTFolder(String folderId);

    GetStatutTdTResponse getTdtStatus(String folderId);

    GetClassificationActesTdtResponse getActesClassification(GetClassificationActesTdtRequest request);
}
