/*
 * Legacy bridge
 * Copyright (C) 2020-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.legacybridge.utils;

import coop.libriciel.iparapheur.internal.model.StepDefinitionDto;
import coop.libriciel.iparapheur.internal.model.StepDefinitionType;
import coop.libriciel.iparapheur.legacy.model.Action;
import coop.libriciel.iparapheur.legacy.model.DeskRepresentation;
import coop.libriciel.iparapheur.legacy.model.State;
import coop.libriciel.iparapheur.legacy.model.Task;
import coop.libriciel.iparapheur.provisioning.model.MetadataType;
import coop.libriciel.iparapheur.provisioning.model.SignatureFormat;
import coop.libriciel.legacybridge.generated.*;
import coop.libriciel.legacybridge.models.AuthData;
import coop.libriciel.legacybridge.models.parapheur.CustomTask;
import coop.libriciel.legacybridge.models.parapheur.Folder;
import lombok.extern.log4j.Log4j2;
import org.apache.commons.lang3.StringUtils;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import org.modelmapper.ModelMapper;
import org.springframework.http.HttpStatus;
import org.springframework.util.CollectionUtils;
import org.springframework.web.reactive.function.client.ClientResponse;
import reactor.core.publisher.Mono;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import java.net.URI;
import java.time.Duration;
import java.util.*;
import java.util.stream.Collectors;

import static coop.libriciel.iparapheur.legacy.model.Action.*;
import static coop.libriciel.iparapheur.legacy.model.State.*;
import static coop.libriciel.iparapheur.provisioning.model.SignatureFormat.AUTO;
import static coop.libriciel.iparapheur.provisioning.model.SignatureFormat.PES_V2;
import static coop.libriciel.legacybridge.utils.MimeTypeUtils.*;
import static java.util.Collections.emptyList;
import static java.util.List.of;
import static java.util.Optional.ofNullable;
import static org.apache.commons.lang3.StringUtils.isEmpty;
import static org.apache.commons.lang3.StringUtils.isNotEmpty;
import static org.springframework.http.MediaType.*;

@Log4j2
public class RequestUtils {


    public static final String ROLE_ARCHIVAGE = "ARCHIVAGE";

    private static ModelMapper initModelMapper() {
        ModelMapper mapper = new ModelMapper();
        mapper.createTypeMap(Task.class, CustomTask.class).addMappings(m -> m.skip(CustomTask::setRead));
        return mapper;
    }

    private static final ModelMapper modelMapper = initModelMapper();

    public static final Duration REQUEST_SHORT_TIMEOUT = Duration.ofSeconds(15);
    public static final Duration REQUEST_LONG_TIMEOUT = Duration.ofSeconds(60);

    public static Mono<HttpStatus> getStatusFromEmptyBodyResponse(ClientResponse clientResponse) {
        return clientResponse.statusCode().isError()
                ? clientResponse.createException().flatMap(Mono::error)
                : Mono.just(HttpStatus.OK);
    }

    public static String getFolderStatus(Folder folder) {
        List<CustomTask> editableStepList = folder.getStepList()
                .stream()
                .map(t -> modelMapper.map(t, CustomTask.class))
                .toList();

        editableStepList
                .stream()
                .filter(step -> step.getAction() != null)
                .filter(step -> step.getAction().equals(SIGNATURE))
                .peek(s -> log.debug("s.readByUserIds : {}", s.getReadByUserIds()))
                .forEach(step -> folder.getStepList()
                        .stream()
                        .filter(t -> t.getWorkflowIndex() != null)
                        .filter(t -> t.getWorkflowIndex().equals(step.getWorkflowIndex()))
                        .filter(t -> t.getAction() != null)
                        .filter(t -> t.getAction().equals(READ))
                        .findFirst()
                        .ifPresent(task -> step.setRead(true))
                );

        CustomTask currentStep = editableStepList
                .stream()
                .filter(step -> step.getAction() != null)
                .filter(step -> step.getState() != null)
                .filter(step -> !step.getAction().equals(DELETE))
                .filter(step -> step.getState().equals(PENDING))
                .findFirst()
                .orElse(null);

        if (currentStep != null) {
            return mapActionToStatus(currentStep);
        }

        List<CustomTask> validatedSteps = editableStepList
                .stream()
                .filter(step -> step.getAction() != null)
                .filter(step -> step.getState() != null)
                .filter(step -> !step.getAction().equals(DELETE))
                .filter(step -> step.getState().equals(VALIDATED) || step.getState().equals(REJECTED))
                .toList();

        if (!validatedSteps.isEmpty()) {
            CustomTask lastStep = validatedSteps.get(validatedSteps.size() - 1);
            return mapActionToStatus(lastStep);
        }
        return "Inconnu";
    }

    public static NatureMetaDonnee mapNatureMetadata(MetadataType nature) {
        return switch (nature) {
            case DATE -> NatureMetaDonnee.DATE;
            case INTEGER -> NatureMetaDonnee.INT;
            case FLOAT -> NatureMetaDonnee.FLOAT;
            case BOOLEAN -> NatureMetaDonnee.BOOLEAN;
            default -> NatureMetaDonnee.STRING;
        };
    }

    public static NatureMetaDonnee mapNatureMetadata(coop.libriciel.iparapheur.legacy.model.MetadataType nature) {
        return switch (nature) {
            case DATE -> NatureMetaDonnee.DATE;
            case INTEGER -> NatureMetaDonnee.INT;
            case FLOAT -> NatureMetaDonnee.FLOAT;
            case BOOLEAN -> NatureMetaDonnee.BOOLEAN;
            default -> NatureMetaDonnee.STRING;
        };
    }

    public static String mapActionToStatus(CustomTask step) {
        boolean isRead = step.isRead();
        Action action = step.getAction();
        State state = step.getState();

        if (action == ARCHIVE) {
            return "Archive";
        }

        if (state == SECONDED) {
            return "EnCoursVisa";
        }

        if (state == REJECTED) {
            return switch (action) {
                case VISA -> "RejetVisa";
                case SECURE_MAIL -> "RejetMailSecPastell";
                case SEAL -> "RejetCachet";
                case SIGNATURE, PAPER_SIGNATURE, EXTERNAL_SIGNATURE -> "RejetSignataire";
                default -> "Inconnu";
            };
        }
        if (state == VALIDATED) {
            return switch (action) {
                case VISA, SECOND_OPINION -> "Vise";
                case SEAL -> "CachetOK";
                case SIGNATURE, EXTERNAL_SIGNATURE -> "Signe";
                case PAPER_SIGNATURE -> "SignatairePapier";
                case SECURE_MAIL -> "MailSecPastell";
                case IPNG -> "IPNG";
                case IPNG_RETURN -> "RetourIPNG";
                case UNDO -> "Annulation";
                case TRANSFER -> "Transfert";
                case CREATE -> "Creation";
                case START -> "NonLu";
                case CHAIN -> "Chaine";
                case RECYCLE -> "Recyclage";
                case DELETE -> "Suppression";
                case BYPASS -> "Contournement";
                case UPDATE -> "MiseAjour";
                case READ -> "Lecture";
                default -> "Inconnu";
            };
        }

        if (state.equals(CURRENT) || state.equals(PENDING)) {
            return switch (action) {
                case VISA, SECOND_OPINION -> "EnCoursVisa";
                case SEAL -> "EnCoursCachet";
                case SIGNATURE -> isRead ? "Lu" : "NonLu";
                case EXTERNAL_SIGNATURE -> "Lu";
                case PAPER_SIGNATURE -> "EnCoursSignatairePapier";
                case SECURE_MAIL -> "EnCoursMailSecPastell";
                case IPNG -> "EnCoursIpng";
                default -> "Inconnu";
            };
        }

        if (state.equals(TRANSFERRED)) {
            return switch (action) {
                case VISA -> "Transfert de l'action de visa";
                case SEAL -> "Transfert de l'action de cachet";
                case SIGNATURE -> "Transfert de l'action de signature";
                case EXTERNAL_SIGNATURE -> "Transfert de l'action de signature externe";
                case SECURE_MAIL -> "Transfert de l'action de mail sécurisé";
                default -> "Inconnu";
            };
        }

        return "error fetching state";
    }

    public static String mapActionToAnnotation(Action action) {
        return switch (action) {
            case VISA, SECOND_OPINION -> "Visa sur dossier";
            case SEAL -> "Cachet sur dossier";
            case SIGNATURE, EXTERNAL_SIGNATURE -> "Signature sur dossier";
            case PAPER_SIGNATURE -> "Signature papier sur dossier";
            case REJECT -> "Rejet du dossier";
            case SECURE_MAIL -> "Mail sécurisé";
            case IPNG -> "Ipng";
            case IPNG_RETURN -> "Retour Ipng";
            case UNDO -> "Annulation";
            case TRANSFER -> "Transfert";
            case CREATE, START -> "Création de dossier";
            case ARCHIVE -> "Archivage du dossier";
            case CHAIN -> "Chaine";
            case RECYCLE -> "Recyclage";
            case DELETE -> "Deletion";
            case BYPASS -> "Contournement";
            case UPDATE -> "Modification";
            case READ -> "Lecture";
            default -> "Inconnu";
        };
    }

    public static String mapStatus(String status) {
        return switch (status.toLowerCase(Locale.ROOT)) {
            case "posté" -> DRAFT.toString();
            case "annulé",
                 "refusé",
                 "rejet",
                 "rejet*",
                 "rejetcachet",
                 "rejetmailsec",
                 "rejetmailsecpastell",
                 "rejettransmission",
                 "rejetsignataire",
                 "rejetsignataireexterne",
                 "rejetvisa" -> REJECTED.toString();
            case "validé",
                 "archive" -> FINISHED.toString();
            case "encoursmailsecpastell",
                 "encoursvisa",
                 "encourscachet",
                 "pretcachet",
                 "encourssignature",
                 "encourssignatureexterne",
                 "nonlu" -> PENDING.toString();
            default -> null;
        };
    }

    public static boolean isSpecificSearch(String status) {
        return StringUtils.containsAny(
                status.toLowerCase(),
                "cachet",
                "visa",
                "signature",
                "signataire",
                "mailsecpastell",
                "signatureexterne",
                "nonlu"
        );
    }

    public static String getCurrentTaskMessage(Task task) {
        if (task.getAction() == null) return "Action Inconnue";

        String taskName = switch (task.getAction()) {
            case VISA, SECOND_OPINION -> "Visa";
            case SEAL -> "cachet serveur.";
            case SIGNATURE, PAPER_SIGNATURE, EXTERNAL_SIGNATURE -> "signature";
            case SECURE_MAIL -> "MailSecurisePastell";
            default -> "Action Inconnue";
        };

        if (CollectionUtils.isEmpty(task.getDesks())) return "Action Inconnue";

        return "Dossier déposé sur le bureau "
                + task.getDesks().get(0).getName()
                + " pour "
                + taskName;
    }

    public static String getCurrentTaskStatus(Action action) {
        return switch (action) {
            case VISA, SECOND_OPINION -> "EnCoursVisa";
            case SEAL -> "PretCachet";
            case SIGNATURE, EXTERNAL_SIGNATURE -> "NonLu";
            case SECURE_MAIL -> "EnCoursMailSecurisePastell";
            default -> "EnCours";
        };
    }

    public static String mapActionToRole(@Nullable StepDefinitionType type) {
        if (type == null) return "INCONNU";

        return switch (type) {
            case VISA -> "VISEUR";
            case SEAL, SIGNATURE, EXTERNAL_SIGNATURE -> "SIGNATAIRE";
            case SECURE_MAIL -> "EMAIL SECURISE";
            case IPNG -> "IPNG";
            case ARCHIVE -> ROLE_ARCHIVAGE;
        };
    }

    public static void setMessageRetourToUnauthorized(MessageRetour messageRetour) {
        messageRetour.setCodeRetour("KO");
        messageRetour.setMessage("Authentification invalide.");
        messageRetour.setSeverite("ERROR");
    }

    public static String createFolderTests(CreerDossierRequest request, SignatureFormat signatureFormat) {

        if (request.getDossierID() == null && request.getDossierTitre() == null) {
            return "Requete incomplete, renseignez 'DossierId' ou 'DossierTitre'.";
        } else if (request.getTypeTechnique() == null || request.getTypeTechnique().isEmpty()) {
            return "Requete incomplete, champ obligatoire 'TypeTechnique' manquant ou vide.";
        } else if (request.getSousType() == null || request.getSousType().isEmpty()) {
            return "Requete incomplete, champ obligatoire 'SousType' manquant ou vide.";
        } else if (isNotEmpty(request.getDateLimite()) && !request.getDateLimite().matches("^\\d{4}-(0[1-9]|1[012])-(0[1-9]|[12]\\d|3[01])$")) {
            return "La DateLimite doit être au format AAAA-MM-JJ, au lieu de: " + request.getDateLimite();
        } else if (request.getVisibilite() == null) {
            return "Requete incomplete, champ obligatoire 'Visibilite' manquant ou invalide.";
        } else if (request.getDossierID() != null && !request.getDossierID().matches("^[^:&\"£*/<>?%|+;\\\\]*$")) {
            return "Le champ dossierID ('" + request.getDossierID() + "') a une syntaxe incorrecte.";
        }

        String mainDocumentType;

        if (request.getDocumentPrincipal() != null) {
            mainDocumentType = request.getDocumentPrincipal().getContentType();
        } else {
            return "Requete incorrecte, pas de document principal.";
        }

        List<String> acceptedMimeTypes = signatureFormat == PES_V2
                ? of(TEXT_XML_VALUE, APPLICATION_XML_VALUE, READER_PES_V2_VALUE)
                : of(
                APPLICATION_PDF_VALUE,
                TEXT_PLAIN_VALUE,
                IMAGE_JPEG_VALUE,
                IMAGE_PNG_VALUE,
                IMAGE_GIF_VALUE,
                APPLICATION_VND_OASIS_OPENDOCUMENT_TEXT_VALUE,
                APPLICATION_RTF_VALUE,
                TEXT_RTF_VALUE,
                APPLICATION_X_RTF_VALUE,
                APPLICATION_VND_OASIS_OPENDOCUMENT_SPREADSHEET_VALUE,
                APPLICATION_VND_OASIS_OPENDOCUMENT_PRESENTATION_VALUE,
                APPLICATION_MSWORD_VALUE,
                APPLICATION_VND_MS_EXCEL_VALUE,
                APPLICATION_VND_MS_POWERPOINT_VALUE,
                APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSINGML_DOCUMENT_VALUE,
                APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_SHEET_VALUE,
                APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_PRESENTATION_VALUE
        );

        boolean mainDocumentInvalid = request.getDocumentPrincipal() == null
                || request.getDocumentPrincipal().getValue() == null
                || request.getDocumentPrincipal().getValue().length == 0;

        boolean isPdfInvalid = isEmpty(mainDocumentType) || mainDocumentInvalid;

        if (isPdfInvalid)
            return "Requete incorrecte, le document principal n'est pas un PDF valide";

        boolean isMimeTypeIncorrect = !signatureFormat.equals(AUTO)
                && acceptedMimeTypes
                .stream()
                .noneMatch(mainDocumentType::contains);

        if (isMimeTypeIncorrect)
            return "Requete incomplete, le type MIME '" + mainDocumentType + "' de 'DocumentPrincipal' n'est pas accepté.";

        ofNullable(request.getDocumentsSupplementaires())
                .ifPresent(additionalDocs -> additionalDocs
                        .getDocAnnexe()
                        .removeIf(doc -> doc == null
                                || isEmpty(doc.getMimetype())
                                || !acceptedMimeTypes.contains(doc.getMimetype())
                                || doc.getFichier().getValue() == null
                                || doc.getFichier().getValue().length == 0));


        ofNullable(request.getDocumentsAnnexes())
                .ifPresent(docAnnexes -> docAnnexes
                        .getDocAnnexe()
                        .removeIf(doc -> doc == null
                                || isEmpty(doc.getMimetype())
                                || !acceptedMimeTypes.contains(doc.getMimetype())
                                || doc.getFichier().getValue() == null
                                || doc.getFichier().getValue().length == 0));

        return "OK";
    }

    public static void setSearchFoldersResponseData(
            List<Folder> populatedFolders,
            RechercherDossiersRequest request,
            RechercherDossiersResponse response,
            AuthData authData) {

        populatedFolders
                .stream()
                .filter(Objects::nonNull)
                .forEach(folder -> {
                    LogDossier logDossier = new LogDossier();
                    logDossier.setStatus("Non lu");
                    logDossier.setAnnotation("");
                    logDossier.setTimestamp(null);

                    List<Task> stepList = folder.getStepList();
                    if (CollectionUtils.isEmpty(stepList)) {
                        log.warn("A folder was retrieved without any steps : {}", folder.getId());
                        stepList = emptyList();
                    }

                    List<Task> validatedSteps = stepList
                            .stream()
                            .filter(step -> step.getState() != null && step.getState().equals(State.VALIDATED))
                            .toList();

                    if (!validatedSteps.isEmpty()) {
                        Task lastStep = validatedSteps.get(validatedSteps.size() - 1);
                        logDossier.setAnnotation(isNotEmpty(lastStep.getPublicAnnotation()) ? lastStep.getPublicAnnotation() : "");
                        logDossier.setStatus(RequestUtils.getFolderStatus(folder));
                        try {
                            GregorianCalendar gregorianCalendar = new GregorianCalendar();
                            if (lastStep.getBeginDate() != null) {
                                gregorianCalendar.setTime(Date.from(lastStep.getBeginDate().toInstant()));
                                logDossier.setTimestamp(DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar));
                            }
                        } catch (DatatypeConfigurationException e) {
                            // Do nothing
                        }
                    } else {
                        try {
                            GregorianCalendar gregorianCalendar = new GregorianCalendar();
                            logDossier.setTimestamp(DatatypeFactory.newInstance().newXMLGregorianCalendar(gregorianCalendar));
                        } catch (DatatypeConfigurationException e) {
                            // Do nothing
                        }
                    }

                    Optional<Task> pendingTask = folder
                            .getStepList()
                            .stream()
                            .filter(step -> Objects.equals(step.getState(), PENDING) || Objects.equals(step.getState(), CURRENT))
                            .findFirst();

                    logDossier.setAccessible(
                            pendingTask
                                    .filter(task -> task.getDesks() != null)
                                    .filter(task -> task.getDesks()
                                            .stream()
                                            .map(DeskRepresentation::getId)
                                            .anyMatch(deskId -> Objects.equals(deskId, authData.getDesk().getId()))
                                    )
                                    .map(task -> "OK")
                                    .orElse("KO")
                    );

                    stepList.stream()
                            .filter(task -> isNotEmpty(task.getPublicAnnotation()))
                            .forEach(task -> logDossier.setAnnotation(task.getPublicAnnotation()));

                    String logDossierName = isNotEmpty(folder.getLegacyId()) && !folder.getLegacyId().equals("null")
                            ? folder.getLegacyId()
                            : folder.getId();

                    logDossier.setNom(logDossierName);

                    boolean isStatusSearchIncorrect = isNotEmpty(request.getStatus())
                            && RequestUtils.isSpecificSearch(request.getStatus())
                            && !Objects.equals(request.getStatus(), logDossier.getStatus());

                    if (isStatusSearchIncorrect) return;

                    response.getLogDossier().add(logDossier);
                });
    }

    public static String parseStepParapheur(@NotNull StepDefinitionDto step) {
        List<coop.libriciel.iparapheur.internal.model.DeskRepresentation> validators = step.getValidatingDesks();

        if (validators == null) return "";

        return validators.stream()
                .map(coop.libriciel.iparapheur.internal.model.DeskRepresentation::getName)
                .collect(Collectors.joining(", "));
    }

    /**
     * Natively, Springboot's WebClient doesn't seem to use URI objects in an optimal way,
     * and ends up keeping un-encoded characters in the query part.
     * <p>
     * As it turns up, the toASCIIString method of the URI objects returns a string where all said characters *are* correctly encoded.
     * So we just return the result of this method as is, it seems to do the trick
     *
     * @return a string cleaned of any non-url-compliant char
     */
    public static String encodeSpecificCharsForUri(URI uri) {
        return uri.toASCIIString();
    }
}
