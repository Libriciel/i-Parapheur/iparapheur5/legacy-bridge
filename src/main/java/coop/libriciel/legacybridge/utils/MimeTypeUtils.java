/*
 * Legacy bridge
 * Copyright (C) 2020-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.legacybridge.utils;


import java.util.HashMap;
import java.util.Map;

import static org.springframework.http.MediaType.*;


public class MimeTypeUtils {

    public static final String PKCS7_VALUE = "application/pkcs7-signature";

    public static final String TEXT_RTF_VALUE = "text/rtf";
    public static final String APPLICATION_RTF_VALUE = "application/rtf";
    public static final String APPLICATION_X_RTF_VALUE = "application/x_rtf";

    public static final String READER_PES_V2_VALUE = "readerpesv2";
    public static final String IMAGE_TIFF_VALUE = "image/tiff";
    public static final String APPLICATION_ZIP_VALUE = "application/zip";

    public static final String APPLICATION_MSWORD_VALUE = "application/msword";
    public static final String APPLICATION_VND_MS_EXCEL_VALUE = "application/vnd.ms-excel";
    public static final String APPLICATION_VND_MS_POWERPOINT_VALUE = "application/vnd.ms-powerpoint";

    public static final String APPLICATION_VND_OASIS_OPENDOCUMENT_TEXT_VALUE = "application/vnd.oasis.opendocument.text";
    public static final String APPLICATION_VND_OASIS_OPENDOCUMENT_PRESENTATION_VALUE = "application/vnd.oasis.opendocument.presentation";
    public static final String APPLICATION_VND_OASIS_OPENDOCUMENT_SPREADSHEET_VALUE = "application/vnd.oasis.opendocument.spreadsheet";

    public static final String APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSINGML_DOCUMENT_VALUE = "application/vnd.openxmlformats-officedocument.wordprocessingml.document";
    public static final String APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_SHEET_VALUE = "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet";
    public static final String APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_PRESENTATION_VALUE = "application/vnd.openxmlformats-officedocument.presentationml.presentation";


    private static final Map<String, String> _mimeTypeToSingleExtensionMap;


    static {
        _mimeTypeToSingleExtensionMap = new HashMap<>();

        _mimeTypeToSingleExtensionMap.put(TEXT_RTF_VALUE, "rtf");
        _mimeTypeToSingleExtensionMap.put(APPLICATION_RTF_VALUE, "rtf");
        _mimeTypeToSingleExtensionMap.put(APPLICATION_X_RTF_VALUE, "rtf");

        _mimeTypeToSingleExtensionMap.put(READER_PES_V2_VALUE, "xml");
        _mimeTypeToSingleExtensionMap.put(TEXT_XML_VALUE, "xml");
        _mimeTypeToSingleExtensionMap.put(APPLICATION_XML_VALUE, "xml");
        _mimeTypeToSingleExtensionMap.put(TEXT_HTML_VALUE, "html");
        _mimeTypeToSingleExtensionMap.put(APPLICATION_XHTML_XML_VALUE, "xhtml");

        _mimeTypeToSingleExtensionMap.put(APPLICATION_PDF_VALUE, "pdf");
        _mimeTypeToSingleExtensionMap.put(APPLICATION_ZIP_VALUE, "zip");

        _mimeTypeToSingleExtensionMap.put(APPLICATION_MSWORD_VALUE, "doc");
        _mimeTypeToSingleExtensionMap.put(APPLICATION_VND_MS_EXCEL_VALUE, "xls");
        _mimeTypeToSingleExtensionMap.put(APPLICATION_VND_MS_POWERPOINT_VALUE, "ppt");

        _mimeTypeToSingleExtensionMap.put(APPLICATION_VND_OASIS_OPENDOCUMENT_TEXT_VALUE, "odt");
        _mimeTypeToSingleExtensionMap.put(APPLICATION_VND_OASIS_OPENDOCUMENT_PRESENTATION_VALUE, "odp");
        _mimeTypeToSingleExtensionMap.put(APPLICATION_VND_OASIS_OPENDOCUMENT_SPREADSHEET_VALUE, "ods");

        _mimeTypeToSingleExtensionMap.put(APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_WORDPROCESSINGML_DOCUMENT_VALUE, "docx");
        _mimeTypeToSingleExtensionMap.put(APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_SPREADSHEETML_SHEET_VALUE, "xlsx");
        _mimeTypeToSingleExtensionMap.put(APPLICATION_VND_OPENXMLFORMATS_OFFICEDOCUMENT_PRESENTATIONML_PRESENTATION_VALUE, "pptx");

        _mimeTypeToSingleExtensionMap.put(IMAGE_JPEG_VALUE, "jpg");
        _mimeTypeToSingleExtensionMap.put(IMAGE_PNG_VALUE, "png");
        _mimeTypeToSingleExtensionMap.put(IMAGE_GIF_VALUE, "gif");
        _mimeTypeToSingleExtensionMap.put(IMAGE_TIFF_VALUE, "tif");

        _mimeTypeToSingleExtensionMap.put(PKCS7_VALUE, "p7s");

    }


    private MimeTypeUtils() {
        throw new IllegalStateException("Utility class");
    }


    /**
     * Retrieve the extension to use for the passed mime type.
     * <p>
     * Return null if this mmime-type is not handled yet.
     *
     * @param mimetype the file mime-type, low-case.
     * @return the extension to use for this mime type, or null.
     */
    public static String getExtensionFromMimeType(String mimetype) {
        return _mimeTypeToSingleExtensionMap.get(mimetype);
    }

}
