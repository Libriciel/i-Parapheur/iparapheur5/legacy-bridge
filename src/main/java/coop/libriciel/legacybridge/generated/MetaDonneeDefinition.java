/*
 * Legacy bridge
 * Copyright (C) 2020-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.legacybridge.generated;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * Définition de méta donnée dans le dictionnaire du modèle de contenu dynamique.
 * 
 * <p>Classe Java pour MetaDonneeDefinition complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="MetaDonneeDefinition">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="nomCourt" type="{http://www.adullact.org/spring-ws/iparapheur/1.0}TypeNomCourt"/>
 *         &lt;element name="nomLong" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="nature" type="{http://www.adullact.org/spring-ws/iparapheur/1.0}NatureMetaDonnee"/>
 *         &lt;element name="obligatoire" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *         &lt;element name="valeurPossible" type="{http://www.w3.org/2001/XMLSchema}string" maxOccurs="unbounded" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MetaDonneeDefinition", propOrder = {
    "nomCourt",
    "nomLong",
    "nature",
    "obligatoire",
    "valeurPossible"
})
public class MetaDonneeDefinition {

    @XmlElement(required = true)
    protected String nomCourt;
    @XmlElement(required = true)
    protected String nomLong;
    @XmlElement(required = true)
    @XmlSchemaType(name = "string")
    protected NatureMetaDonnee nature;
    protected Boolean obligatoire;
    protected List<String> valeurPossible;

    /**
     * Obtient la valeur de la propriété nomCourt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomCourt() {
        return nomCourt;
    }

    /**
     * Définit la valeur de la propriété nomCourt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomCourt(String value) {
        this.nomCourt = value;
    }

    /**
     * Obtient la valeur de la propriété nomLong.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomLong() {
        return nomLong;
    }

    /**
     * Définit la valeur de la propriété nomLong.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomLong(String value) {
        this.nomLong = value;
    }

    /**
     * Obtient la valeur de la propriété nature.
     * 
     * @return
     *     possible object is
     *     {@link NatureMetaDonnee }
     *     
     */
    public NatureMetaDonnee getNature() {
        return nature;
    }

    /**
     * Définit la valeur de la propriété nature.
     * 
     * @param value
     *     allowed object is
     *     {@link NatureMetaDonnee }
     *     
     */
    public void setNature(NatureMetaDonnee value) {
        this.nature = value;
    }

    /**
     * Obtient la valeur de la propriété obligatoire.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isObligatoire() {
        return obligatoire;
    }

    /**
     * Définit la valeur de la propriété obligatoire.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setObligatoire(Boolean value) {
        this.obligatoire = value;
    }

    /**
     * Gets the value of the valeurPossible property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the valeurPossible property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getValeurPossible().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link String }
     * 
     * 
     */
    public List<String> getValeurPossible() {
        if (valeurPossible == null) {
            valeurPossible = new ArrayList<String>();
        }
        return this.valeurPossible;
    }

}
