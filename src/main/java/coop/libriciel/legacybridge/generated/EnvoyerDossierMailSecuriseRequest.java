/*
 * Legacy bridge
 * Copyright (C) 2020-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.legacybridge.generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="dossier" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="rcptTo" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="subject" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="message" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="password" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="sendPassword" type="{http://www.w3.org/2001/XMLSchema}boolean" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "dossier",
    "rcptTo",
    "subject",
    "message",
    "password",
    "sendPassword"
})
@XmlRootElement(name = "EnvoyerDossierMailSecuriseRequest")
public class EnvoyerDossierMailSecuriseRequest {

    @XmlElement(required = true)
    protected String dossier;
    @XmlElement(required = true)
    protected String rcptTo;
    @XmlElement(required = true)
    protected String subject;
    @XmlElement(required = true)
    protected String message;
    protected String password;
    protected Boolean sendPassword;

    /**
     * Obtient la valeur de la propriété dossier.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDossier() {
        return dossier;
    }

    /**
     * Définit la valeur de la propriété dossier.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDossier(String value) {
        this.dossier = value;
    }

    /**
     * Obtient la valeur de la propriété rcptTo.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getRcptTo() {
        return rcptTo;
    }

    /**
     * Définit la valeur de la propriété rcptTo.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setRcptTo(String value) {
        this.rcptTo = value;
    }

    /**
     * Obtient la valeur de la propriété subject.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubject() {
        return subject;
    }

    /**
     * Définit la valeur de la propriété subject.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubject(String value) {
        this.subject = value;
    }

    /**
     * Obtient la valeur de la propriété message.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessage() {
        return message;
    }

    /**
     * Définit la valeur de la propriété message.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessage(String value) {
        this.message = value;
    }

    /**
     * Obtient la valeur de la propriété password.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPassword() {
        return password;
    }

    /**
     * Définit la valeur de la propriété password.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPassword(String value) {
        this.password = value;
    }

    /**
     * Obtient la valeur de la propriété sendPassword.
     * 
     * @return
     *     possible object is
     *     {@link Boolean }
     *     
     */
    public Boolean isSendPassword() {
        return sendPassword;
    }

    /**
     * Définit la valeur de la propriété sendPassword.
     * 
     * @param value
     *     allowed object is
     *     {@link Boolean }
     *     
     */
    public void setSendPassword(Boolean value) {
        this.sendPassword = value;
    }

}
