/*
 * Legacy bridge
 * Copyright (C) 2020-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.legacybridge.generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="TypeTechnique" type="{http://www.adullact.org/spring-ws/iparapheur/1.0}TypeTechnique"/>
 *         &lt;element name="SousType" type="{http://www.adullact.org/spring-ws/iparapheur/1.0}SousType"/>
 *         &lt;element name="EmailEmetteur" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DossierID" type="{http://www.adullact.org/spring-ws/iparapheur/1.0}DossierID"/>
 *         &lt;element name="FichierPES" type="{http://www.adullact.org/spring-ws/iparapheur/1.0}TypeDoc"/>
 *         &lt;element name="VisuelPDF" type="{http://www.adullact.org/spring-ws/iparapheur/1.0}TypeDoc"/>
 *         &lt;element name="XPathPourSignaturePES" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AnnotationPublique" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AnnotationPrivee" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Visibilite" type="{http://www.adullact.org/spring-ws/iparapheur/1.0}Visibilite"/>
 *         &lt;element name="DateLimite" type="{http://www.adullact.org/spring-ws/iparapheur/1.0}date_AAAA-MM-JJ"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {

})
@XmlRootElement(name = "CreerDossierPESRequest")
public class CreerDossierPESRequest {

    @XmlElement(name = "TypeTechnique", required = true)
    protected String typeTechnique;
    @XmlElement(name = "SousType", required = true)
    protected String sousType;
    @XmlElement(name = "EmailEmetteur", required = true)
    protected String emailEmetteur;
    @XmlElement(name = "DossierID", required = true)
    protected String dossierID;
    @XmlElement(name = "FichierPES", required = true)
    protected TypeDoc fichierPES;
    @XmlElement(name = "VisuelPDF", required = true)
    protected TypeDoc visuelPDF;
    @XmlElement(name = "XPathPourSignaturePES", required = true)
    protected String xPathPourSignaturePES;
    @XmlElement(name = "AnnotationPublique", required = true, nillable = true)
    protected String annotationPublique;
    @XmlElement(name = "AnnotationPrivee", required = true, nillable = true)
    protected String annotationPrivee;
    @XmlElement(name = "Visibilite", required = true)
    @XmlSchemaType(name = "string")
    protected Visibilite visibilite;
    @XmlElement(name = "DateLimite", required = true, nillable = true)
    protected String dateLimite;

    /**
     * Obtient la valeur de la propriété typeTechnique.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeTechnique() {
        return typeTechnique;
    }

    /**
     * Définit la valeur de la propriété typeTechnique.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeTechnique(String value) {
        this.typeTechnique = value;
    }

    /**
     * Obtient la valeur de la propriété sousType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSousType() {
        return sousType;
    }

    /**
     * Définit la valeur de la propriété sousType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSousType(String value) {
        this.sousType = value;
    }

    /**
     * Obtient la valeur de la propriété emailEmetteur.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailEmetteur() {
        return emailEmetteur;
    }

    /**
     * Définit la valeur de la propriété emailEmetteur.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailEmetteur(String value) {
        this.emailEmetteur = value;
    }

    /**
     * Obtient la valeur de la propriété dossierID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDossierID() {
        return dossierID;
    }

    /**
     * Définit la valeur de la propriété dossierID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDossierID(String value) {
        this.dossierID = value;
    }

    /**
     * Obtient la valeur de la propriété fichierPES.
     * 
     * @return
     *     possible object is
     *     {@link TypeDoc }
     *     
     */
    public TypeDoc getFichierPES() {
        return fichierPES;
    }

    /**
     * Définit la valeur de la propriété fichierPES.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeDoc }
     *     
     */
    public void setFichierPES(TypeDoc value) {
        this.fichierPES = value;
    }

    /**
     * Obtient la valeur de la propriété visuelPDF.
     * 
     * @return
     *     possible object is
     *     {@link TypeDoc }
     *     
     */
    public TypeDoc getVisuelPDF() {
        return visuelPDF;
    }

    /**
     * Définit la valeur de la propriété visuelPDF.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeDoc }
     *     
     */
    public void setVisuelPDF(TypeDoc value) {
        this.visuelPDF = value;
    }

    /**
     * Obtient la valeur de la propriété xPathPourSignaturePES.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXPathPourSignaturePES() {
        return xPathPourSignaturePES;
    }

    /**
     * Définit la valeur de la propriété xPathPourSignaturePES.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXPathPourSignaturePES(String value) {
        this.xPathPourSignaturePES = value;
    }

    /**
     * Obtient la valeur de la propriété annotationPublique.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnnotationPublique() {
        return annotationPublique;
    }

    /**
     * Définit la valeur de la propriété annotationPublique.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnnotationPublique(String value) {
        this.annotationPublique = value;
    }

    /**
     * Obtient la valeur de la propriété annotationPrivee.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnnotationPrivee() {
        return annotationPrivee;
    }

    /**
     * Définit la valeur de la propriété annotationPrivee.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnnotationPrivee(String value) {
        this.annotationPrivee = value;
    }

    /**
     * Obtient la valeur de la propriété visibilite.
     * 
     * @return
     *     possible object is
     *     {@link Visibilite }
     *     
     */
    public Visibilite getVisibilite() {
        return visibilite;
    }

    /**
     * Définit la valeur de la propriété visibilite.
     * 
     * @param value
     *     allowed object is
     *     {@link Visibilite }
     *     
     */
    public void setVisibilite(Visibilite value) {
        this.visibilite = value;
    }

    /**
     * Obtient la valeur de la propriété dateLimite.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateLimite() {
        return dateLimite;
    }

    /**
     * Définit la valeur de la propriété dateLimite.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateLimite(String value) {
        this.dateLimite = value;
    }

}
