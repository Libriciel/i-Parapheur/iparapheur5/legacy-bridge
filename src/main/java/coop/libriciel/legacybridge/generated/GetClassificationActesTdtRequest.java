/*
 * Legacy bridge
 * Copyright (C) 2020-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.legacybridge.generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="TypeTechnique" type="{http://www.adullact.org/spring-ws/iparapheur/1.0}TypeTechnique" minOccurs="0"/>
 *         &lt;element name="DemanderMiseAJourTdt" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {
    "typeTechnique",
    "demanderMiseAJourTdt"
})
@XmlRootElement(name = "GetClassificationActesTdtRequest")
public class GetClassificationActesTdtRequest {

    @XmlElement(name = "TypeTechnique")
    protected String typeTechnique;
    @XmlElement(name = "DemanderMiseAJourTdt")
    protected String demanderMiseAJourTdt;

    /**
     * Obtient la valeur de la propriété typeTechnique.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeTechnique() {
        return typeTechnique;
    }

    /**
     * Définit la valeur de la propriété typeTechnique.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeTechnique(String value) {
        this.typeTechnique = value;
    }

    /**
     * Obtient la valeur de la propriété demanderMiseAJourTdt.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDemanderMiseAJourTdt() {
        return demanderMiseAJourTdt;
    }

    /**
     * Définit la valeur de la propriété demanderMiseAJourTdt.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDemanderMiseAJourTdt(String value) {
        this.demanderMiseAJourTdt = value;
    }

}
