/*
 * Legacy bridge
 * Copyright (C) 2020-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.legacybridge.generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour MessageRetour complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType name="MessageRetour">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="codeRetour" type="{http://www.adullact.org/spring-ws/iparapheur/1.0}codeRetour"/>
 *         &lt;element name="message" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="severite" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *       &lt;/sequence>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "MessageRetour", propOrder = {
    "codeRetour",
    "message",
    "severite"
})
public class MessageRetour {

    @XmlElement(required = true)
    protected String codeRetour;
    @XmlElement(required = true)
    protected String message;
    @XmlElement(required = true)
    protected String severite;

    /**
     * Obtient la valeur de la propriété codeRetour.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getCodeRetour() {
        return codeRetour;
    }

    /**
     * Définit la valeur de la propriété codeRetour.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setCodeRetour(String value) {
        this.codeRetour = value;
    }

    /**
     * Obtient la valeur de la propriété message.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMessage() {
        return message;
    }

    /**
     * Définit la valeur de la propriété message.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMessage(String value) {
        this.message = value;
    }

    /**
     * Obtient la valeur de la propriété severite.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSeverite() {
        return severite;
    }

    /**
     * Définit la valeur de la propriété severite.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSeverite(String value) {
        this.severite = value;
    }

}
