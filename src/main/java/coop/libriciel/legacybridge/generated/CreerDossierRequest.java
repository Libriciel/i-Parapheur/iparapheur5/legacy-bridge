/*
 * Legacy bridge
 * Copyright (C) 2020-2025 Libriciel-SCOP
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

package coop.libriciel.legacybridge.generated;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlSchemaType;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Classe Java pour anonymous complex type.
 * 
 * <p>Le fragment de schéma suivant indique le contenu attendu figurant dans cette classe.
 * 
 * <pre>
 * &lt;complexType>
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;all>
 *         &lt;element name="TypeTechnique" type="{http://www.adullact.org/spring-ws/iparapheur/1.0}TypeTechnique"/>
 *         &lt;element name="SousType" type="{http://www.adullact.org/spring-ws/iparapheur/1.0}SousType"/>
 *         &lt;element name="EmailEmetteur" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DossierID" type="{http://www.adullact.org/spring-ws/iparapheur/1.0}DossierID"/>
 *         &lt;element name="DossierTitre" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="DocumentPrincipal" type="{http://www.adullact.org/spring-ws/iparapheur/1.0}TypeDoc"/>
 *         &lt;element name="NomDocPrincipal" type="{http://www.w3.org/2001/XMLSchema}string" minOccurs="0"/>
 *         &lt;element name="SignatureDocPrincipal" type="{http://www.adullact.org/spring-ws/iparapheur/1.0}TypeDoc" minOccurs="0"/>
 *         &lt;element name="VisuelPDF" type="{http://www.adullact.org/spring-ws/iparapheur/1.0}TypeDoc"/>
 *         &lt;element name="DocumentsSupplementaires" type="{http://www.adullact.org/spring-ws/iparapheur/1.0}TypeDocAnnexes" minOccurs="0"/>
 *         &lt;element name="DocumentsAnnexes" type="{http://www.adullact.org/spring-ws/iparapheur/1.0}TypeDocAnnexes" minOccurs="0"/>
 *         &lt;element name="MetaData" type="{http://www.adullact.org/spring-ws/iparapheur/1.0}TypeMetaDonnees" minOccurs="0"/>
 *         &lt;element name="XPathPourSignatureXML" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="MetaDataTdtACTES" type="{http://www.adullact.org/spring-ws/iparapheur/1.0}metaDataTdtACTES"/>
 *         &lt;element name="AnnotationPublique" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="AnnotationPrivee" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="Visibilite" type="{http://www.adullact.org/spring-ws/iparapheur/1.0}Visibilite"/>
 *         &lt;element name="DateLimite" type="{http://www.adullact.org/spring-ws/iparapheur/1.0}date_AAAA-MM-JJ"/>
 *         &lt;element name="CircuitObligatoire" type="{http://www.adullact.org/spring-ws/iparapheur/1.0}TypeCircuit" minOccurs="0"/>
 *       &lt;/all>
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "", propOrder = {

})
@XmlRootElement(name = "CreerDossierRequest")
public class CreerDossierRequest {

    @XmlElement(name = "TypeTechnique", required = true)
    protected String typeTechnique;
    @XmlElement(name = "SousType", required = true)
    protected String sousType;
    @XmlElement(name = "EmailEmetteur")
    protected String emailEmetteur;
    @XmlElement(name = "DossierID", required = true)
    protected String dossierID;
    @XmlElement(name = "DossierTitre")
    protected String dossierTitre;
    @XmlElement(name = "DocumentPrincipal", required = true)
    protected TypeDoc documentPrincipal;
    @XmlElement(name = "NomDocPrincipal")
    protected String nomDocPrincipal;
    @XmlElement(name = "SignatureDocPrincipal")
    protected TypeDoc signatureDocPrincipal;
    @XmlElement(name = "VisuelPDF", required = true, nillable = true)
    protected TypeDoc visuelPDF;
    @XmlElement(name = "DocumentsSupplementaires")
    protected TypeDocAnnexes documentsSupplementaires;
    @XmlElement(name = "DocumentsAnnexes")
    protected TypeDocAnnexes documentsAnnexes;
    @XmlElement(name = "MetaData")
    protected TypeMetaDonnees metaData;
    @XmlElement(name = "XPathPourSignatureXML", required = true, nillable = true)
    protected String xPathPourSignatureXML;
    @XmlElement(name = "MetaDataTdtACTES", required = true, nillable = true)
    protected MetaDataTdtACTES metaDataTdtACTES;
    @XmlElement(name = "AnnotationPublique", required = true, nillable = true)
    protected String annotationPublique;
    @XmlElement(name = "AnnotationPrivee", required = true, nillable = true)
    protected String annotationPrivee;
    @XmlElement(name = "Visibilite", required = true)
    @XmlSchemaType(name = "string")
    protected Visibilite visibilite;
    @XmlElement(name = "DateLimite", required = true, nillable = true)
    protected String dateLimite;
    @XmlElement(name = "CircuitObligatoire")
    protected TypeCircuit circuitObligatoire;

    /**
     * Obtient la valeur de la propriété typeTechnique.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getTypeTechnique() {
        return typeTechnique;
    }

    /**
     * Définit la valeur de la propriété typeTechnique.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setTypeTechnique(String value) {
        this.typeTechnique = value;
    }

    /**
     * Obtient la valeur de la propriété sousType.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSousType() {
        return sousType;
    }

    /**
     * Définit la valeur de la propriété sousType.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSousType(String value) {
        this.sousType = value;
    }

    /**
     * Obtient la valeur de la propriété emailEmetteur.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getEmailEmetteur() {
        return emailEmetteur;
    }

    /**
     * Définit la valeur de la propriété emailEmetteur.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setEmailEmetteur(String value) {
        this.emailEmetteur = value;
    }

    /**
     * Obtient la valeur de la propriété dossierID.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDossierID() {
        return dossierID;
    }

    /**
     * Définit la valeur de la propriété dossierID.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDossierID(String value) {
        this.dossierID = value;
    }

    /**
     * Obtient la valeur de la propriété dossierTitre.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDossierTitre() {
        return dossierTitre;
    }

    /**
     * Définit la valeur de la propriété dossierTitre.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDossierTitre(String value) {
        this.dossierTitre = value;
    }

    /**
     * Obtient la valeur de la propriété documentPrincipal.
     * 
     * @return
     *     possible object is
     *     {@link TypeDoc }
     *     
     */
    public TypeDoc getDocumentPrincipal() {
        return documentPrincipal;
    }

    /**
     * Définit la valeur de la propriété documentPrincipal.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeDoc }
     *     
     */
    public void setDocumentPrincipal(TypeDoc value) {
        this.documentPrincipal = value;
    }

    /**
     * Obtient la valeur de la propriété nomDocPrincipal.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getNomDocPrincipal() {
        return nomDocPrincipal;
    }

    /**
     * Définit la valeur de la propriété nomDocPrincipal.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setNomDocPrincipal(String value) {
        this.nomDocPrincipal = value;
    }

    /**
     * Obtient la valeur de la propriété signatureDocPrincipal.
     * 
     * @return
     *     possible object is
     *     {@link TypeDoc }
     *     
     */
    public TypeDoc getSignatureDocPrincipal() {
        return signatureDocPrincipal;
    }

    /**
     * Définit la valeur de la propriété signatureDocPrincipal.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeDoc }
     *     
     */
    public void setSignatureDocPrincipal(TypeDoc value) {
        this.signatureDocPrincipal = value;
    }

    /**
     * Obtient la valeur de la propriété visuelPDF.
     * 
     * @return
     *     possible object is
     *     {@link TypeDoc }
     *     
     */
    public TypeDoc getVisuelPDF() {
        return visuelPDF;
    }

    /**
     * Définit la valeur de la propriété visuelPDF.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeDoc }
     *     
     */
    public void setVisuelPDF(TypeDoc value) {
        this.visuelPDF = value;
    }

    /**
     * Obtient la valeur de la propriété documentsSupplementaires.
     * 
     * @return
     *     possible object is
     *     {@link TypeDocAnnexes }
     *     
     */
    public TypeDocAnnexes getDocumentsSupplementaires() {
        return documentsSupplementaires;
    }

    /**
     * Définit la valeur de la propriété documentsSupplementaires.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeDocAnnexes }
     *     
     */
    public void setDocumentsSupplementaires(TypeDocAnnexes value) {
        this.documentsSupplementaires = value;
    }

    /**
     * Obtient la valeur de la propriété documentsAnnexes.
     * 
     * @return
     *     possible object is
     *     {@link TypeDocAnnexes }
     *     
     */
    public TypeDocAnnexes getDocumentsAnnexes() {
        return documentsAnnexes;
    }

    /**
     * Définit la valeur de la propriété documentsAnnexes.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeDocAnnexes }
     *     
     */
    public void setDocumentsAnnexes(TypeDocAnnexes value) {
        this.documentsAnnexes = value;
    }

    /**
     * Obtient la valeur de la propriété metaData.
     * 
     * @return
     *     possible object is
     *     {@link TypeMetaDonnees }
     *     
     */
    public TypeMetaDonnees getMetaData() {
        return metaData;
    }

    /**
     * Définit la valeur de la propriété metaData.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeMetaDonnees }
     *     
     */
    public void setMetaData(TypeMetaDonnees value) {
        this.metaData = value;
    }

    /**
     * Obtient la valeur de la propriété xPathPourSignatureXML.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getXPathPourSignatureXML() {
        return xPathPourSignatureXML;
    }

    /**
     * Définit la valeur de la propriété xPathPourSignatureXML.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setXPathPourSignatureXML(String value) {
        this.xPathPourSignatureXML = value;
    }

    /**
     * Obtient la valeur de la propriété metaDataTdtACTES.
     * 
     * @return
     *     possible object is
     *     {@link MetaDataTdtACTES }
     *     
     */
    public MetaDataTdtACTES getMetaDataTdtACTES() {
        return metaDataTdtACTES;
    }

    /**
     * Définit la valeur de la propriété metaDataTdtACTES.
     * 
     * @param value
     *     allowed object is
     *     {@link MetaDataTdtACTES }
     *     
     */
    public void setMetaDataTdtACTES(MetaDataTdtACTES value) {
        this.metaDataTdtACTES = value;
    }

    /**
     * Obtient la valeur de la propriété annotationPublique.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnnotationPublique() {
        return annotationPublique;
    }

    /**
     * Définit la valeur de la propriété annotationPublique.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnnotationPublique(String value) {
        this.annotationPublique = value;
    }

    /**
     * Obtient la valeur de la propriété annotationPrivee.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getAnnotationPrivee() {
        return annotationPrivee;
    }

    /**
     * Définit la valeur de la propriété annotationPrivee.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setAnnotationPrivee(String value) {
        this.annotationPrivee = value;
    }

    /**
     * Obtient la valeur de la propriété visibilite.
     * 
     * @return
     *     possible object is
     *     {@link Visibilite }
     *     
     */
    public Visibilite getVisibilite() {
        return visibilite;
    }

    /**
     * Définit la valeur de la propriété visibilite.
     * 
     * @param value
     *     allowed object is
     *     {@link Visibilite }
     *     
     */
    public void setVisibilite(Visibilite value) {
        this.visibilite = value;
    }

    /**
     * Obtient la valeur de la propriété dateLimite.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateLimite() {
        return dateLimite;
    }

    /**
     * Définit la valeur de la propriété dateLimite.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateLimite(String value) {
        this.dateLimite = value;
    }

    /**
     * Obtient la valeur de la propriété circuitObligatoire.
     * 
     * @return
     *     possible object is
     *     {@link TypeCircuit }
     *     
     */
    public TypeCircuit getCircuitObligatoire() {
        return circuitObligatoire;
    }

    /**
     * Définit la valeur de la propriété circuitObligatoire.
     * 
     * @param value
     *     allowed object is
     *     {@link TypeCircuit }
     *     
     */
    public void setCircuitObligatoire(TypeCircuit value) {
        this.circuitObligatoire = value;
    }

}
