# Legacy bridge

## A bridge to rule them all

### Test with postman :

- URL : POST : http://localhost:2758/ws

```xml
<?xml version="1.0" encoding="utf-8"?>
<Envelope xmlns="http://schemas.xmlsoap.org/soap/envelope/">
  <Body>
    <postDocumentRequest xmlns="http://localhost:2758/document">
        <base64EncodedDocument>Any text you want</base64EncodedDocument>
    </postDocumentRequest>
  </Body>
</Envelope>
```

- Response :

```xml
<SOAP-ENV:Envelope xmlns:SOAP-ENV="http://schemas.xmlsoap.org/soap/envelope/">
    <SOAP-ENV:Header/>
    <SOAP-ENV:Body>
        <ns2:postDocumentResponse xmlns:ns2="http://localhost:2758/document">
            <ns2:responseStatus>Any text you want</ns2:responseStatus>
        </ns2:postDocumentResponse>
    </SOAP-ENV:Body>
</SOAP-ENV:Envelope>
```
