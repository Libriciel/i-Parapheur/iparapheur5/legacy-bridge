# Paramètres fonctionnels spécifiques

## Legacy V4 retry

Il est possible de configurer Legacy-bridge pour qu'il essaie de relancer certaines requêtes vers une instance iparapheur v4. 


### Propriété à renseigner

| Clé                                       | Fonction                                                                                                                                                                                |  Example                                               |
|-------------------------------------------|-----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|--------------------------------------------------------|
| `service.parapheur.legacy-parapheur-url`  | URL webservice du parapheur v4 sur lequel les requêtes seront relancées.<br/> ce doit-être l'url "secure" complète, elle doit être préfixée par "https" et doit finir par ws-iparapheur | https://secure-iparapheur47.some.host.fr/ws-iparapheur |  


### Application des paramètres

Dans le dossier `/opt/iparapheur/current/`, créer un fichier `application-override-legacy-bridge.yml` (s'il n'existe pas déjà),  
et y ajouter la clé en respectant le format YAML.

Exemple :

```yaml
service.parapheur.legacy-parapheur-url: https://secure-iparapheur47.other.example.fr/ws-iparapheur
```

Créer ensuite un fichier `docker-compose.override.yml` (s'il n'existe pas déjà),  
et y ajouter le fichier de paramétrage précédemment créé au service Legacy-bridge, en respectant le format YAML :

```yaml
services:

  # (...)

  legacy-bridge:
    volumes:
      - ./application-override-legacy-bridge.yml:/application.yml

  # (...)
```  

Si un de ces fichiers n'existait pas encore, ou si `application-override-legacy-bridge.yml` n'était pas encore monté comme volume sur le conteneur de core, alors il est nécessaire de
détruire et recréer le conteneur pour que la modification soit prise en compte :

```bash
docker compose stop legacy-bridge
docker compose rm -f legacy-bridge
docker compose up -d legacy-bridge
```

Après cette mise en place initiale, à chaque ajout/modification d'un paramètre, il suffit de redémarrer le conteneur pour la prise en compte des nouveaux paramètres.

```bash
docker compose restart legacy-bridge
```


### Paramètrage fonctionnel

Le paramètrage de ce rebond vers une v4 nécessite un paramètrage spécifique : il faut *impérativement* que l'utilisateur avec lequel le webservice se connecte
existe sur les deux parapheurs - et ce avec *le même mot de passe*.

Seules les requêtes de récupérations et de suppressions seront réessayées (getdossier, gethistodossier, archiverdossier), et ce uniquement si l'objet de la requête n'est pas trouvé sur le parapheur v5.
