#
# Legacy bridge
# Copyright (C) 2020-2025 Libriciel-SCOP
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as
# published by the Free Software Foundation, either version 3 of the
# License, or (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.
#

FROM hubdocker.libriciel.fr/eclipse-temurin:17.0.12_7-jre-alpine@sha256:8bc1d9babb88b2ca206b3cb3533e15cd90bce3d6e7c49baf09c456538b95c65d

HEALTHCHECK --interval=5s --timeout=2s --retries=60 \
  CMD wget --quiet --tries=1 --proxy off -O- http://localhost:8080/ws-iparapheur/actuator/health | grep UP || exit 1

# Open Containers Initiative parameters
ARG CI_COMMIT_REF_NAME=""
ARG CI_COMMIT_SHA=""
ARG CI_PIPELINE_CREATED_AT=""
# Non-standard and/or deprecated variables, that are still widely used.
# If it is already set in the FROM image, it has to be overridden.
MAINTAINER Libriciel SCOP
LABEL maintainer="Libriciel SCOP"
LABEL org.label-schema.build-date="$CI_PIPELINE_CREATED_AT"
LABEL org.label-schema.name="legacy-bridge"
LABEL org.label-schema.schema-version="1.0"
LABEL org.label-schema.vendor="Libriciel SCOP"
# Open Containers Initiative's image specifications
LABEL org.opencontainers.image.authors="Libriciel SCOP"
LABEL org.opencontainers.image.created="$CI_PIPELINE_CREATED_AT"
LABEL org.opencontainers.image.description="Retrocompatibility for the SOAP requests, and fallback v4 linkage"
LABEL org.opencontainers.image.licenses="GNU Affero GPL v3"
LABEL org.opencontainers.image.revision="$CI_COMMIT_SHA"
LABEL org.opencontainers.image.source="registry.libriciel.fr/public/signature/legacy-bridge"
LABEL org.opencontainers.image.title="legacy-bridge"
LABEL org.opencontainers.image.vendor="Libriciel SCOP"
LABEL org.opencontainers.image.version="$CI_COMMIT_REF_NAME"

# Needed to fix 'Fontconfig warning: ignoring C.UTF-8: not a valid language tag'
ENV LANG fr_FR.UTF-8

VOLUME /tmp

ADD build/libs/legacy-bridge-*.war legacy-bridge.war
ENV JAVA_OPTS=""

COPY docker-entrypoint.sh /usr/local/bin/
RUN chmod +x /usr/local/bin/docker-entrypoint.sh
RUN ln -s /usr/local/bin/docker-entrypoint.sh /

ENTRYPOINT [ "docker-entrypoint.sh" ]
